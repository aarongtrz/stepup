<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::get('/', 		'Site\SiteController@landing'					)->name('landing');
Route::get('/maestros', 'Site\SiteController@maestros'					)->name('maestros');
Route::get('/clases', 	'Site\SiteController@clases'					)->name('clases');
Route::get('/eventos', 	'Site\SiteController@eventos'					)->name('eventos');
Route::get('/galeria/{photo_page?}', 	'Site\SiteController@galeria'					)->name('galeria');
Route::get('/galeria/detalle/{slug}', 	'Site\SiteController@galeriaDetalle'					)->name('galeria.detalle');
Route::get('/horarios', 'Site\HorariosController@index'					)->name('horarios');
Route::get('/contacto', 'Site\SiteController@contacto'					)->name('contacto');
Route::post('/contacto', 'Site\SiteController@contacto_email'			)->name('contacto_email');
Route::get('/descarga-horario', 'Site\SiteController@descargaPdf'			)->name('descargaPdf');

Route::prefix('admin')->namespace('Admin')->name('admin.')->middleware('auth')->group(function () {
	
	Route::prefix('clases')->name('clases.')->group(function () {
		Route::get('/', 'ClasesController@index')->name('index');
		Route::get('/editar/{id?}', 'ClasesController@edit')->name('edit');
		Route::match(['post', 'put'], '/save', 'ClasesController@save')->name('save');
		Route::delete('/delete', 'ClasesController@delete')->name('delete');
	});
	
	Route::prefix('maestros')->name('maestros.')->group(function () {
		Route::get('/', 'MaestrosController@index')->name('index');
		Route::get('/editar/{id?}', 'MaestrosController@edit')->name('edit');
		Route::match(['post', 'put'], '/save', 'MaestrosController@save')->name('save');
		Route::delete('/delete', 'MaestrosController@delete')->name('delete');
	});
	
	Route::prefix('horarios')->name('horarios.')->group(function () {
		Route::get('/', 'HorariosController@index')->name('index');
		Route::get('/editar/{id?}', 'HorariosController@edit')->name('edit');
		Route::match(['post', 'put'], '/save', 'HorariosController@save')->name('save');
		Route::delete('/delete', 'HorariosController@delete')->name('delete');
	});
	
	Route::prefix('eventos')->name('eventos.')->group(function () {
		Route::get('/', 'EventosController@index')->name('index');
		Route::get('/editar/{id?}', 'EventosController@edit')->name('edit');
		Route::match(['post', 'put'], '/save', 'EventosController@save')->name('save');
		Route::delete('/delete', 'EventosController@delete')->name('delete');
	});
	
});

/*Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});*/

Route::get('/pass', function () {
    dd(bcrypt('stepup'));
});
