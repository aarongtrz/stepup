$(document).ready(function(){
	eliminarClic();
});

//ALERTAS PARA ACCIONES CON AJAX Y OTROS EVENTOS
function alertInfo(mensaje, recargarPagina){
	$.alert({
		title: 'Mensaje',
	    content: mensaje,
	    icon: 'fas fa-exclamation',
	    type: 'dark',
	    typeAnimated: true,
	    buttons: {
	        close: {
	            text: 'Cerrar',
	            action: function(){
		            if(recargarPagina){
			            location.reload();
		            }
	            }
	        }
	    },
	    theme: 'modern'
	});
}

function alertSuccess(mensaje, recargarPagina){
	$.alert({
		title: 'Correcto',
	    content: mensaje,
	    icon: 'fas fa-check',
	    type: 'green',
	    typeAnimated: true,
	    buttons: {
	        close: {
	            text: 'Cerrar',
	            action: function(){
		            if(recargarPagina){
			            location.reload();
		            }
	            }
	        }
	    },
	    theme: 'modern'
	});
}

function alertError(mensaje){
	$.alert({
		title: 'Error',
	    content: mensaje,
	    type: 'red',
	    icon: "fas fa-times",
	    typeAnimated: true,
	    buttons: {
	        close: {
	            text: 'Cerrar',
	            action: function(){
	            }
	        }
	    },
	    theme: 'modern'
	});
}

function eliminarClic(){
	$(document).on('click', '.eliminar', function(){
		var id = $(this).data('id');
		confirmacionEliminar(id);
	});
}

function confirmacionEliminar(id){
	$.confirm({
	    title: 'Confirma',
	    content: '¿Está seguro que quiere eliminar?',
	    buttons: {
	        confirm: {
		        text: 'Sí',
		        action: function(){
			        eliminar(id);
		        },
		        btnClass: 'btn-purple-alert'
	        },
	        cancel: {
		        text: 'Cancelar',
		        action: function(){
			        
		        }
	        }
	    },
	    theme: 'modern',
	    icon: 'fa fa-question'
	});
}

function alertErrorPhp(htmlError){
    
    $.alert({
		title: 'Error',
	    content: htmlError,
	    type: 'red',
	    typeAnimated: true,
	    icon: "fas fa-times",
	    columnClass: 'col-sm-5',
	    buttons: {
	        close: {
	            text: 'Cerrar',
	            action: function(){
	            }
	        }
	    },
	    theme: 'modern'
	});
}

function alertSuccessPhp(mensaje){
	$.alert({
		title: 'Correcto',
	    content: mensaje,
	    type: 'green',
	    icon: 'fas fa-check',
	    typeAnimated: true,
	    buttons: {
	        close: {
	            text: 'Cerrar',
	            action: function(){
	            }
	        }
	    },
	    theme: 'modern'
	});
}