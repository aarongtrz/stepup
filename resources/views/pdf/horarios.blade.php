<html><head>
		<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600,700" rel="stylesheet">
		<style>
			* 	 { margin: 0;padding: 0; }
			body { font-size: 14px; font-family: 'Josefin Sans', sans-serif; }
			.tabla .header{
				background-color: #f58220;
				font-weight: 600;
				text-transform: uppercase;
				font-size: 16px;
				border: 3px solid #1a1a1a !important;
				text-align: center;
			}
			
			.tabla{
				display: inline-block;
			}
			
			.tabla tbody td{
				border-top: none !important;
				border-bottom: 1px solid #ddd;
			}
			
			.tabla .hora{
				vertical-align: middle;
				background-color: #1a1a1a;
				color: #999999;
				border: 3px solid #1a1a1a;
				width: 85px;
			}
			
			.tabla .nombre-clase{
				text-transform: uppercase;
				color: #fff;
				font-size: 14px;
			}
			
			.tabla .detalle-clase{
				color: #999999;
			}
			
			.tabla .detalle-sala{
				color: #fff;
				text-decoration: underline;
			}
			
			.bg-black{
				background-color: #1a1a1a;
				border: 3px solid #1a1a1a !important;
				
			}
			
			td.bg-black,
			td.hora{
				vertical-align: middle !important;
				text-align: center;
			}
			
			.dnone{
				display: none;
			}
			
			.horarios{
				margin-bottom: 80px;
			}
			
			.page-break {
			    page-break-after: always;
			}
			
			.page-avoid{
				page-break-inside: avoid;
			}
			
			.page-break-after{
				page-break-after: avoid;
			}
			
			.contenedor-sucursal{
				padding-top: 50px;
				padding-bottom: 50px;
				padding-left: 25px;
				padding-right: 25px;
				margin: 0 auto;
			}
			
			.tabla table,
			.tabla,
			table{
				vertical-align: top;
			}
			
			.sucursal{
				display: block;
			    margin-left: auto;
			    margin-right: auto;
			    width: 90%;
			    margin-bottom: 20px;
			}
		</style>
	</head><body>
		<div class="contenedor-sucursal">
			@php $contador = 1; @endphp
			@foreach($sucursales as $sucursal)
			@if($sucursal->horarios->isNotEmpty())
			<div class="sucursal">
			<h2 style="display: block; margin-bottom: 60px; text-align: center; font-size: 28px; color: #f58220; text-transform: uppercase;">{{ $sucursal->nombre }}</h2>
				@foreach($dias as $dia)
				<div class="tabla">
					<table class="table" bgcolor="#1a1a1a">
			            <thead class="">
			                <tr>
				                <th class="header" colspan="2" class="text-center"><b>{{ $dia->nombre }}</b></th>
			                </tr>
			            </thead>
			            @if($dia->horarios->isNotEmpty())
			                @if($dia->horarios->contains('sucursal_id', $sucursal->id))
			                <tbody class="">
				                @foreach($dia->horarios as $horario)
				                @if($horario->sucursal_id == $sucursal->id)
					                <tr class="">
						                <td class="hora {{ $sucursal->id }}" rowspan="{{ $horario->clases->count() + 1 }}">
							                <b>{{ substr($horario->hora_inicial, 0, -3) }} </b>
							                a
							                <b>{{ substr($horario->hora_final, 0, -3) }}</b>
						                </td>
					                </tr>
					                @foreach($horario->clases as $clase)
					                <tr class="">
						                <td class="bg-black">
							                <span class="nombre-clase"><b>{{ $clase->nombre }}</b></span>
							                <br>
							                @if( $clase->detalle!="")
							                <span class="detalle-clase">{{ $clase->detalle }}</span>
							                <br>
							                @endif
							                <span class="detalle-sala">
							                	@if($clase->pivot->sala != "") 
							                		S{{ $clase->pivot->sala }}
							                	@else
							                		{{ $clase->pivot->sala }}
							                	@endif
							                </span>
							               
						                </td>
					                </tr>
					                @endforeach
					            @endif
				                @endforeach
			                </tbody>
			                @endif
			            @endif
			        </table>
				</div>
		        @endforeach
			</div>
		    @endif
	        @endforeach
		</div>	
	</body></html>