<!DOCTYPE html>
<html lang="es">
	<head>
		
		<!-- METAS -->
		<title>Step Up | @yield('titulo')</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<meta name="robots" content="index,follow" />
		
		
		<!-- SOCIAL METADATA-->
		<meta property="og:title" content="" />
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="" />
		<meta property="og:url" content="" />
		<meta property="og:description" content="" />
		<meta property="og:image" content="" /> <!-- 1024x1024-->
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:title" content="">
		<meta name="twitter:description" content="">
		<meta name="twitter:image" content=""> <!-- 600x315-->
		
		<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/auth/bootstrap.min.css') }}">
	<!--===============================================================================================-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/auth/animate.css') }}">
	<!--===============================================================================================-->	
		<link rel="stylesheet" type="text/css" href="{{ asset('css/auth/hamburgers.min.css') }}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/auth/animsition.min.css') }}">
	<!--===============================================================================================-->
		<link rel="stylesheet" type="text/css" href="{{ asset('css/auth/util.css') }}">
		<link rel="stylesheet" type="text/css" href="{{ asset('css/auth/main.css') }}">
	<!--===============================================================================================-->
		<link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700" rel="stylesheet"> 
		
		@yield('css')
		<style>
			.container-login100::before{
				background-color: rgba(29, 29, 29, 0.9);
			}
			
			.wrap-login100{
				background: #f58220;
			}
			
			.login100-form-btn:hover, login100-form-btn:active, login100-form-btn:focus{
				background-color: #fff !important;
				background: #fff !important;
				color: #000;
			}
			
			.login100-form-btn::before{
				border-radius: 0px;
			}
			
			.login100-form-btn{
				border-radius: 0px;
			}
		</style>
	</head>
	
	
	<body>
	
		<div class="limiter">
			<div class="container-login100" style="background-image: url('{{ asset("images/auth/bg-auth.jpg") }}');">
				<div class="wrap-login100">
					@yield('contenido')
				</div>
			</div>
		</div>
		
	
		<div id="dropDownSelect1"></div>
		<!--===============================================================================================-->
		<script src="{{ asset('js/auth/jquery-3.2.1.min.js') }}"></script>
	<!--===============================================================================================-->
		<script src="{{ asset('js/auth/animsition.min.js') }}"></script>
	<!--===============================================================================================-->
		<script src="{{ asset('js/auth/popper.js') }}"></script>
		<script src="{{ asset('js/auth/bootstrap.min.js') }}"></script>
	<!--===============================================================================================-->
		<script src="{{ asset('js/auth/countdowntime.js') }}"></script>
	<!--===============================================================================================-->
		<script src="{{ asset('js/auth/main.js') }}"></script>
		
		@yield('js')
	</body>
	
</html>