<!doctype html>
<html lang="es">

	<head>
	    <title>Step Up Admin | @yield('titulo')</title>
	    <!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
	
	    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	    
	    <!-- CSRF Token -->
		<meta name="csrf-token" content="{{ csrf_token() }}">
	
	    <!--     Fonts and icons     -->
	    <link href="https://fonts.googleapis.com/css?family=Nunito:300,400,600,700" rel="stylesheet"> 
	    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Material+Icons" />
	    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
	
	    <!-- Material Dashboard CSS -->
	    <link rel="stylesheet" href="{{ asset('css/admin/material-dashboard.css') }}">
	    
	    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">
	    
	    <link rel="stylesheet" href="{{ asset('css/admin/custom.css') }}">
		
		@yield('css')
	</head>
	
	<body>
		@yield('modals')
	    <div class="wrapper ">
	        @include('layouts.admin.sidebar')
	        
	        <div class="main-panel">
	            @include('layouts.admin.navbar')
	            
	            <div class="content">
		            <div class="container-fluid">
	            	@yield('content')
		            </div>
	            </div>
	        </div>
	    </div>
	
	    <!--   Core JS Files   -->
	    <script src="{{ asset('js/admin/jquery.min.js') }}"></script>
	    <script src="{{ asset('js/admin/popper.min.js') }}"></script>
	    <script src="{{ asset('js/admin/bootstrap-material-design.min.js') }}"></script>
	    
		<!-- jQuery confirm -->
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
	
	    <!-- Material Dashboard Core initialisations of plugins and Bootstrap Material Design Library -->
	    <script src="{{ asset('js/admin/material-dashboard.min.js?v=2.1.0') }}"></script>
	    
	    <script src="{{ asset('js/admin/custom.js') }}" type="text/javascript"></script>
	    
	    @if($errors->any())
	    <script>
		    $(document).ready(function(){
				crearMensajeErrorPhp(); 
		    });
		    function crearMensajeErrorPhp(){
			    var htmlError = '';
				
				htmlError += 'Por favor, verifique los siguientes errores:<br><br><ul>';
			    @foreach ($errors->all() as $error)
			        htmlError += '<li>' + "{{ $error }}" + '</li>';
			    @endforeach
			    htmlError += '</ul>';
			    
			    alertErrorPhp(htmlError);
		    }
	    </script>
	    @endif
	    @if(session("mensaje"))
		<script>
			$(document).ready(function(){
				crearMensajeSuccessPhp(); 
		    });
			function crearMensajeSuccessPhp(){
				var mensaje = "{{ session('mensaje') }}";
				alertSuccessPhp(mensaje);
			}
		</script>
		@endif
	    
	    @yield('js')
	</body>

</html>