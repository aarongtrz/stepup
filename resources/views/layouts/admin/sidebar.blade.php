<div class="sidebar" data-color="orange" data-background-color="black" data-image="https://demos.creative-tim.com/material-dashboard/assets/img/sidebar-3.jpg">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

Tip 2: you can also add an image using data-image tag
-->
    <div class="logo">
        <a href="#" class="simple-text logo-normal">
			<img src="{{ asset('images/logo-transparente.png') }}" class="img-responsive" style="width: 150px;">
		</a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            {{-- <li class="nav-item  ">
                <a class="nav-link" href="./dashboard.html">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li> --}}
            <li class="nav-item @yield('active_clases')">
                <a class="nav-link" href="{{ route('admin.clases.index') }}">
                    <i class="material-icons">person</i>
                    <p>Clases</p>
                </a>
            </li>
            <li class="nav-item @yield('active_maestros')">
                <a class="nav-link" href="{{ route('admin.maestros.index') }}">
                    <i class="material-icons">content_paste</i>
                    <p>Maestros</p>
                </a>
            </li>
            <li class="nav-item @yield('active_horarios')">
                <a class="nav-link" href="{{ route('admin.horarios.index') }}">
                    <i class="material-icons">library_books</i>
                    <p>Horario</p>
                </a>
            </li>
            <li class="nav-item @yield('active_eventos')">
                <a class="nav-link" href="{{ route('admin.eventos.index') }}">
                    <i class="material-icons">bubble_chart</i>
                    <p>Eventos</p>
                </a>
            </li>
        </ul>
    </div>
</div>