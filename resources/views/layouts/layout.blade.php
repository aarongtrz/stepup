<!DOCTYPE html>
<html>
	<head>
		
		<!-- METAS -->
		<title>@yield('title', 'Step Up')</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta name="author" content="">
		<meta name="robots" content="index,follow" />
		
		<!-- FAVICON -->
		<link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicon/apple-touch-icon.png') }}">
		<link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicon/favicon-32x32.png') }}">
		<link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicon/favicon-16x16.png') }}">
		<link rel="manifest" href="{{ asset('favicon/manifest.json') }}">
		<link rel="mask-icon" href="{{ asset('favicon/safari-pinned-tab.svg') }}" color="#809343">
		<link rel="shortcut icon" href="{{ asset('favicon/favicon.ico') }}">
		<meta name="msapplication-config" content="{{ asset('favicon/browserconfig.xml') }}">
		<meta name="theme-color" content="#ffffff">
		
		
		<!-- SOCIAL METADATA-->
		<meta property="og:title" content="The Next Step To Your Passion" />
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="Step Up" />
		<meta property="og:url" content="{{ route('landing') }}" />
		<meta property="og:description" content="Step Up nace a partir del sueño de una gran coreógrafa y apasionada del baile; Mariana Esteban, quien después de 10 años de experiencia ha alcanzado un excelente nivel de calidad y creatividad dentro de sus coreografías y quien se ha caracterizado por lograr compartir un poco de su pasión a sus alumnos y lograr que ellos proyecten y disfruten al presentarse en cualquier escenario." />
		<meta property="og:image" content="{{ asset('images/metas/meta_fb.png') }}" />
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:title" content="The Next Step To Your Passion">
		<meta name="twitter:description" content="Step Up nace a partir del sueño de una gran coreógrafa y apasionada del baile; Mariana Esteban, quien después de 10 años de experiencia ha alcanzado un excelente nivel de calidad y creatividad dentro de sus coreografías y quien se ha caracterizado por lograr compartir un poco de su pasión a sus alumnos y lograr que ellos proyecten y disfruten al presentarse en cualquier escenario.">
		<meta name="twitter:image" content="{{ asset('images/metas/meta_fb.png') }}"> 
		
		<!-- STANDARD CSS-->
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" >
		
		<link rel="stylesheet" href="{{asset('css/custom.css')}}" >
		@yield('css')
	</head>
	
	
	<body>
	
		<nav class="navbar navbar-default ">
	      <div class="container">
	        <div class="navbar-header">
	          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
	            <span class="sr-only">Menú</span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	            <span class="icon-bar"></span>
	          </button>
	          <a class="navbar-brand" href="{{route('landing')}}"><img src="{{asset('images/logo.png')}}" class="logo_menu"></a>
	        </div>
	        <div id="navbar" class="navbar-collapse collapse">
	          <ul class="nav navbar-nav navbar-upper">
	            <li><a href="https://www.facebook.com/StepUpDF/" target="_blank">
			            <span class="fa-stack fa-lg">
						  <i class="fa fa-circle fa-stack-2x"></i>
						  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
						</span>
					</a>
				</li>
				<li><a href="https://www.instagram.com/stepup.danceacademy/" target="_blank">
			            <span class="fa-stack fa-lg">
						  <i class="fa fa-circle fa-stack-2x"></i>
						  <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
						</span>
					</a>
				</li>
	          </ul>
	          <ul class="nav navbar-nav navbar-right">
	            <li><a href="{{route('maestros')}}">Maestros</a></li>
	            <li><a href="{{route('clases')}}">Clases</a></li>
	            <li><a href="{{route('eventos')}}">Eventos</a></li>
	            <li><a href="{{route('galeria')}}">Galería</a></li>
	            <li><a href="{{route('horarios')}}">Horario</a></li>
	            <li><a href="{{route('contacto')}}">Contacto</a></li>
	            
	          </ul>
	        </div>
	      </div>
	    </nav>
	    <div class="spacer"></div>
	    
	    
	    
	    
	    
	    @yield('content')
	    
	    
	    
	    
	    
	    <footer>
		    <div class="footer">
			    <div class="container">
				    <div class="row">
					    <div class="col-sm-12">
						    <img src="{{asset('images/logo-transparente.png')}}" class="img-footer">
					    </div>
					    
					    <div class="col-sm-3">
						    <p class="title">Mapa del sitio</p>
						    <div class="enlaces">
							    <a href="{{route('landing')}}">Quienes Somos</a>
							    <a href="{{route('maestros')}}">Maestros</a>
							    <a href="{{route('eventos')}}">Eventos</a>
							    <a href="{{route('galeria')}}">Galería</a>
							    <a href="{{route('horarios')}}">Horario</a>
							    <a href="{{route('contacto')}}">Contacto</a>
						    </div>
					    </div>
					    <div class="col-sm-3">
					    	<p class="title">Nuestras clases</p>
						    <div class="enlaces">
							    Jazz y Jazz Kids<br>
							    Jazz Fusión<br>
							    Show Dance<br>
							    Técnica<br>
							    Hip Hop<br>
							    MiniKids<br>
							    Cardio Dance
						    </div>
					    </div>
					    <div class="col-sm-3">
						    <p class="title">Sucursales</p>
						    <p>
							   <div class="subtitle">CDMX - Las Águilas</div> 
							   <p>Calzada de las Águilas 453, Col. Águilas. Entre Dr. Cervantes Ahumada y 2da Cda. Águilas. Del. Álvaro Obregón <br> <a style="text-decoration: none; color: inherit;" href="mailto:contactocdxm@stepup.com.mx">contactocdxm@stepup.com.mx</a> </p>
							   
							   <div class="subtitle">CDMX - Bosque Real</div> 
							   <p>Carretera México Huixquilican No. 180 Col. Ex ejidos de San Cristobal.<br> <a style="text-decoration: none; color: inherit;" href="mailto:contactocdxm@stepup.com.mx">contactobr@stepup.com.mx</a> </p>
							   
							   <div class="subtitle">Mérida</div> 
							   <p>C.30 #249 X 73 Y 75 Colonia Montes de Amé <br> <a style="text-decoration: none; color: inherit;" href="mailto:contactocdxm@stepup.com.mx">contactobr@stepup.com.mx</a> </p>
						    </p>
					    </div>
					    <div class="col-sm-3 text-right">
						    <p class="title">&nbsp;</p>
						    
						    <p>&copy; {{date('Y')}} StepUp Dance Studio<br> Todos los derechos reservados</p>
						    
							
						    <div>
							    <a href="#">Aviso de Privacidad</a><br><br>
						    </div>
						    <div>
							    <a href="https://www.facebook.com/StepUpDF/" class="no_underline" target="_blank">
						            <span class="fa-stack fa-lg">
									  <i class="fa fa-circle fa-stack-2x"></i>
									  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
									</span>
								</a>
								<a href="https://www.instagram.com/stepup.danceacademy/" class="no_underline" target="_blank">
						            <span class="fa-stack fa-lg">
									  <i class="fa fa-circle fa-stack-2x"></i>
									  <i class="fa fa-instagram fa-stack-1x fa-inverse"></i>
									</span>
								</a>
						    </div>
					    </div>
					    
					    
				    </div>
			    </div>
		    </div>
	    </footer>
	    
	    
	   
	
	
	</body>

	<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
	<script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" ></script>
	@yield('js')
</html>
