@extends('layouts.layout')

@section('title', 'Eventos | Step Up')


@section('css')
<style>
		.titulo_noticia{
			color: #F58220;
			text-transform: uppercase;
			font-size: 21px;
			font-family: 'Josefin Sans', sans-serif;
			margin-top: 22px;
			margin-bottom: 9px;
		}
		
		.foto_noticia{
			width: 100%;
			height: 300px;
			background-size: cover;
			background-position: center center;
		}
		
		.noticia{
			font-size: 14px;
			cursor: pointer;
		}
		.contenido{
			height: 100px;
			position: relative;
			overflow: hidden;
		}
		.contenido .shadow{
			position: absolute;
			height: 100%;
			width: 100%;
			top: 0;
			left: 0;
			background-image: -webkit-linear-gradient(bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
		    background-image: -webkit-linear-gradient(top, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
		    background-image: linear-gradient(to bottom, rgba(255,255,255,0) 0%, rgba(255,255,255,1) 100%);
		    text-align: center;
		}
		.shadow .btn{
			background-color: #F58220;
			color: #fff;
			margin-top: 56px;
		}
		.pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover{
			background-color: #F58220;
			border-color: #F58220; 
		}
		
		.pagination > li > a, .pagination > li > span{
			color: #F58220;
		}
		
		.pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover{
			color: #F58220;
		}
</style>
@endsection

@section('content')
		<section>
		    <div class="banner">
			    <img src="{{asset('images/banners/eventos.png')}}">
		    </div>
	    </section>
	    
	    <div class="seccion">
		    <div class="container">
			    <div class="row">
				    
				    <div class="col-sm-12 text-center" style="margin-bottom: 30px;"><h1 class="titulo">Próximos eventos</h1></div>
				    
				    @foreach($eventos as $evento)
				    	<div class="col-sm-6">
					    	<div class="noticia" data-toggle="modal" data-target="#evento_{{$evento->id}}">
						    	<div class="foto_noticia" style="background-image: url('{{asset("public/storage/eventos")}}/{{$evento->foto}}')"></div>
						    	<div class="titulo_noticia">{{$evento->titulo}}</div>
						    	<div class="contenido">
							    	{!! $evento->descripcion !!}
							    	<div class="shadow">
								    	<div class="btn btn-default">Leer completo</div>
							    	</div>
						    	</div>
					    	</div>
				    	</div>
				    	
				    	
				    	
				    	
				    	<div class="modal fade" id="evento_{{$evento->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
							<div class="modal-dialog" role="document">
								<div class="modal-content">
								
									<div class="modal-header">
										<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
										<h4 class="modal-title" id="myModalLabel">{{$evento->titulo}}</h4>
									</div>
								
									<div class="modal-body">
										<img src="{{asset("public/storage/eventos")}}/{{$evento->foto}}" style="width: 100%">
										<br><br>
										
										{!!$evento->descripcion!!}
									</div>
								
									<div class="modal-footer">
										<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
									</div>
								
								</div>
							</div>
						</div>
				    	
				    	
				    @endforeach
				    
				    
				    
				    <div class="col-sm-12">
					    <div class="paginacion text-center">
					    	{{ $eventos->links() }}
					    </div>
				    </div>
	    
				    
			    </div>
		    </div>
	    </div>
	    
	    
	    @foreach($eventos as $evento)
	    	
	    @endforeach
	    
	    
	    

@endsection

@section('javascript')

@endsection