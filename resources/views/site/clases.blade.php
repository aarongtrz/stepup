@extends('layouts.layout')

@section('title', 'clases | Step Up')


@section('css')
<style>
		
				
		.clase_foto{
			width: 100%;
			height: 400px;
			background-size: cover;
			background-position: center center;
			background-color: gray;
		}
		
		.directivo  > .container > .row{
			font-size: 0;	
		}
		.directivo  > .container > .row .col-sm-3{
			position: relative;
			float: none;
			display: inline-block;
			vertical-align: top;
		}
		.directivo-elemento{
			font-size: 14px;
			cursor: pointer;
		}
		
		h2{
			text-transform: uppercase;
			text-align: center;
			margin-bottom: 50px;
			font-weight: 300;
			font-size: 18px;
			font-family: 'Josefin Sans', sans-serif;
		}
		
		.directivo  > .container > .row .modal-body{
			font-size: 14px !important;
		}
		
		
		@media(min-width: 992px) and (max-width: 1199px){
			.clase_foto{
				height: 300px;
			}
		}
		
		@media(min-width: 768px) and (max-width: 991px){
			.clase_foto{
				height: 200px;
			}
		}
		
		@media(max-width: 767px){
			.directivo  > .container > .row .col-sm-3{
				width: 50%;
			}
			.clase_foto{
				height: 200px;
			}
		}
		
</style>
@endsection

@section('content')
		<section>
		    <div class="banner">
			    <img src="{{asset('images/banners/galeria.png')}}">
		    </div>
	    </section>
	    
	    <div class="container">
		    <div class="seccion">
			    <div class="caja">
				     <div class="directivo">
						<div class="container">
							<div class="row">
								@php 
									$clase_num=0; 
									$array_clases= array();
								@endphp
								@foreach($clases as $clase)
								
									
									@if(!in_array($clase->nombre, $array_clases))
									@php $clase_num++; @endphp
									<div class="col-sm-3">
										<div class="directivo-elemento" data-toggle="modal" data-target="#clase_{{$clase_num}}">
											<div class="clase_foto" style="background-image: url('{{asset("public/storage/maestros")}}/{{$clase->foto}}')"> </div>
											<div class="photo-title">{{$clase->nombre}}</div>
										</div>
									</div>
									
									<div class="modal fade" id="clase_{{$clase_num}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
										<div class="modal-dialog" role="document">
											<div class="modal-content">
											
												<div class="modal-header">
													<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
													<h4 class="modal-title" id="myModalLabel">{{$clase->nombre}}</h4>
												</div>
											
												<div class="modal-body">
												{!!$clase->descripcion!!}
												</div>
											
												<div class="modal-footer">
													<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
												</div>
											
											</div>
										</div>
									</div>
									@php $array_clases[]=$clase->nombre;		@endphp
									@endif
									
									
									
								@endforeach
							</div>
						</div>
				    </div>
			    </div>
		    </div>
	    </div>

@endsection

@section('js')

	<script>
		
		
		
		
		
	</script>

@endsection