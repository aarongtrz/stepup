@extends('layouts.layout')

@section('title', 'Galería | Step Up')


@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<style>
	.hide_{
		background-color: #fff;
		height: 74px;
		position: relative;
		width: 100%;
	}
	.embedsocial-album{
		margin-top: -87px;
	}
	
	.grid{
		/*background-color: gray;*/
	}
	.grid-item{
		/*width: 150px;*/
	}
	.grid-item img{
		width: 100%;
		display: block;
		margin: 5px;
	}
	
	.mensaje-vacio{
		padding: 20px;
		border: 2px solid #941102;
		color: #941102;
		font-weight: 600;
	}
	
	.slick-list{
		text-align: center;
	}
	
	.imagenes{
		margin-bottom: 50px;
		background-color: #fff;
	}
	
	.videos{
		background-color: #e2e2e2;
	}
	
	.content-fotos{
		opacity: 0;
	}
	
	#loader i{
		font-size: 50px;
		color: #F58220;
	}
	
	.pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover{
		background-color: #F58220;
		border-color: #F58220; 
	}
	
	.pagination > li > a, .pagination > li > span{
		color: #F58220;
	}
	
	.pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover{
		color: #F58220;
	}
	
	@media only screen and (max-width: 767px){
		.grid-item img{
			max-width: 140px;
		}
	}
	
	@media only screen and (min-width: 768px) and (max-width: 991px){
		.grid-item img{
			max-width: 230px;
		}
	}
	
	
	@media screen and (min-width: 1200px){
		.grid-item img{
			max-width: 170px;
		}
	}
</style>
@endsection

@section('content')
		<section>
		    <div class="banner">
			    <img src="{{asset('images/banners/galeria.png')}}">
		    </div>
	    </section>
	    
	    <div class="seccion imagenes">
		   <div class="container">
			   <div class="content-fotos">
				   <div class="row">
					   <div class="col-sm-12">
						   <h2 class="titulo text-center" style="font-size: 40px; font-weight: 600;">Galerías</h2>
					   </div>
				   </div>
				   <div class="row">
					   <div class="col-sm-4">
						   <h3 class="titulo text-center">Step Up Las Águilas</h3>
						   <a href="{{ route('galeria.detalle', ['slug' => 'sucursal-aguilas']) }}"><img src="{{ asset('images/albums/sucursal_cdmx.jpg') }}" class="img-responsive"></a>
					   </div>
					   <div class="col-sm-4">
						   <h3 class="titulo text-center">Step Up Bosque Real</h3>
						   <a href="{{ route('galeria.detalle', ['slug' => 'bosque-real']) }}"><img src="{{ asset('images/albums/sucursal_cdmx.jpg') }}" class="img-responsive"></a>
					   </div>
					   <div class="col-sm-4">
						   <h3 class="titulo text-center">Step Up Mérida</h3>
						   <a href="{{ route('galeria.detalle', ['slug' => 'sucursal-merida']) }}"><img src="{{ asset('images/albums/sucursal_merida.jpg') }}" class="img-responsive"></a>
					   </div>
				   </div>
			   </div>
			</div>
	    </div>
	    <div class="seccion videos">
		   @if(!empty($videos))
				<h3 class="titulo text-center">Videos</h3>
				<br><br>
				<div id="videos">
					@foreach($videos as $video)
					<div class="flickr-video">
						<video width="95%" controls>
							<source src="{{ $video['source'] }}" type="video/mp4">
							Tu navegador no soporta HTML5
						</video>
					</div>
					@endforeach
				</div>
			@else
				<h3>Por el momento no se encontraron fotos</h3>	
			@endif
	    </div>
	    
	    {{-- <div class="seccion">
		    <div class="container">
			    <div class="hide_"></div>
			    <div class='embedsocial-album' data-ref="aa6bb8d2fc45e492cd240f99fa06c47af8f0f550"></div><script>(function(d, s, id){var js; if (d.getElementById(id)) {return;} js = d.createElement(s); js.id = id; js.src = "https://embedsocial.com/embedscript/ei.js"; d.getElementsByTagName("head")[0].appendChild(js);}(document, "script", "EmbedSocialScript"));</script> 
		    </div>
		    
		    
	    </div> --}}

@endsection

@section('js')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script>
	$(document).ready(function(){
		//iniciarGaleria();
		iniciarSlick();
	});
	
	function iniciarGaleria(){
		$('.grid').masonry({
			// options
			itemSelector: '.grid-item',
			
			percentPosition: true
		});
	}
	
	$(window).load(function(){	
		iniciarGaleria();
		$('#loader').css('display', 'none');
		$('.content-fotos').css('opacity', '1');
	});
	
	function iniciarSlick(){
		$("#videos").slick({
			dots: true,
			arrows: false,
			autoplay: false,
			centerMode: true,
			centerPadding: '100px',
			slidesToShow: 1,
		});
	}
</script>
@endsection