@extends('layouts.layout')

@section('title', 'Galería | Step Up')


@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.css" />
<style>
	.hide_{
		background-color: #fff;
		height: 74px;
		position: relative;
		width: 100%;
	}
	.embedsocial-album{
		margin-top: -87px;
	}
	
	.grid{
		/*background-color: gray;*/
	}
	.grid-item{
		/*width: 150px;*/
	}
	.grid-item img{
		width: 100%;
		display: block;
		margin: 5px;
	}
	
	.mensaje-vacio{
		padding: 20px;
		border: 2px solid #941102;
		color: #941102;
		font-weight: 600;
	}
	
	.slick-list{
		text-align: center;
	}
	
	.imagenes{
		margin-bottom: 50px;
		background-color: #fff;
	}
	
	.videos{
		background-color: #e2e2e2;
	}
	
	.content-fotos{
		opacity: 0;
	}
	
	#loader i{
		font-size: 50px;
		color: #F58220;
	}
	
	.pagination > .active > a, .pagination > .active > a:focus, .pagination > .active > a:hover, .pagination > .active > span, .pagination > .active > span:focus, .pagination > .active > span:hover{
		background-color: #F58220;
		border-color: #F58220; 
	}
	
	.pagination > li > a, .pagination > li > span{
		color: #F58220;
	}
	
	.pagination > li > a:focus, .pagination > li > a:hover, .pagination > li > span:focus, .pagination > li > span:hover{
		color: #F58220;
	}
	
	@media only screen and (max-width: 767px){
		.grid-item img{
			max-width: 140px;
		}
	}
	
	@media only screen and (min-width: 768px) and (max-width: 991px){
		.grid-item img{
			max-width: 230px;
		}
	}
	
	
	@media screen and (min-width: 1200px){
		.grid-item img{
			max-width: 370px;
		}
	}
</style>
@endsection

@section('content')
		<section>
		    <div class="banner">
			    <img src="{{asset('images/banners/galeria.png')}}">
		    </div>
	    </section>
	    
	    <div class="seccion imagenes">
		   <div class="container">
			   <div id="loader">
				   <div class="row ">
					   <div class="col-sm-12 text-center">
						   <i class="fa fa-circle-o-notch fa-spin"></i>
					   </div>
				   </div>
			   </div>
			   <div class="content-fotos">
				    @if($flickr_imagenes->stat == 'ok')
						<h2 class="titulo text-center" style="font-size: 40px; font-weight: 600;">{{ ucfirst($flickr_imagenes->photoset['title']) }}</h3>
						<br><br>
						@if(!empty($flickr_imagenes->photoset['photo']))
						<div class="row">
							<div class="grid">
							@foreach($flickr_imagenes->photoset['photo'] as $photo)
								<div class="grid-item">
									<a data-fancybox="gallery" href="{{ $photo['url_l'] }}"><img src="{{ $photo['url_m'] }}"></a>
								</div>
							@endforeach
							</div>
						</div>
						<br><br>
						<div class="row">
							<div class="col-sm-12 text-center">
								
							</div>
						</div>
						@else
						<h3>Por el momento no se encontraron fotos</h3>
						@endif
					@else
						<h3>Por el momento no se encontraron fotos</h3>	
					@endif
			   </div>
			</div>
	    </div>

@endsection

@section('js')
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.min.js"></script>
<script>
	$(document).ready(function(){
		iniciarGaleria();
	});
	
	function iniciarGaleria(){
		$('.grid').masonry({
			// options
			itemSelector: '.grid-item',
			percentPosition: true
		});
	}
	
	$(window).load(function(){	
		iniciarGaleria();
		$('#loader').css('display', 'none');
		$('.content-fotos').css('opacity', '1');
	});
</script>
@endsection