@extends('layouts.layout')

@section('title', 'Maestros | Step Up')


@section('css')
<style>
		
		.buttons{
			text-align: center;
		}
		.buttons .button{
			display: inline-block;
			padding: 2px 1px;
			margin: 0px;
			margin: 0px 60px;
			cursor: pointer; 
		}
		.buttons .button.active{
			border-bottom: 2px solid #F58220;
		}
		
		.cajas .caja{
			display: none;
		}
		
		.maestro_foto{
			width: 100%;
			height: 400px;
			background-size: cover;
			background-position: center center;
			filter: grayscale(100%);
		}
		
		.directivo  > .container > .row{
			font-size: 0;	
		}
		.directivo  > .container > .row .col-sm-3{
			position: relative;
			float: none;
			display: inline-block;
			vertical-align: top;
		}
		.directivo-elemento{
			font-size: 14px;
			cursor: pointer;
		}
		
		h2{
			text-transform: uppercase;
			text-align: center;
			margin-bottom: 50px;
			font-weight: 300;
			font-size: 18px;
			font-family: 'Josefin Sans', sans-serif;
		}
		
		.directivo  > .container > .row .modal-body{
			font-size: 14px !important;
		}
		
		
		@media(min-width: 992px) and (max-width: 1199px){
			.maestro_foto{
				height: 300px;
			}
		}
		
		@media(min-width: 768px) and (max-width: 991px){
			.maestro_foto{
				height: 200px;
			}
		}
		
		@media(max-width: 767px){
			.directivo  > .container > .row .col-sm-3{
				width: 50%;
			}
			.maestro_foto{
				height: 200px;
			}
		}
		
</style>
@endsection

@section('content')
		<section>
		    <div class="banner">
			    <img src="{{asset('images/banners/maestros.png')}}">
		    </div>
	    </section>
	    
	    <div class="container">
		    <div class="seccion">
				
				<div id="maestros">
					<div class="buttons">
					    <div class="button" 	btn="1">
						    <div class="titulo">CDMX</div>
						</div>
					    <div class="button" 		btn="2">
						    <div class="titulo">MÉRIDA</div>
						</div>
				    </div>
				    
				    
				    
				    <div class="cajas">
					    <div class="caja">
						    <div class="directivo">
								<div class="container">
									<div class="row">
										<h2>CDMX Las Águilas</h2>
										@php $maestro_num = 0; @endphp
										@foreach($maestros as $maestro)
											@if($maestro->sucursal->nombre == "CDMX Las Aguilas")
											@php $maestro_num++; @endphp
											<div class="col-sm-3">
												<div class="directivo-elemento">
													<div class="maestro_foto" style="background-image: url('{{asset("public/storage/maestros")}}/{{$maestro->foto}}')"> </div>
													<div class="photo-title">{{$maestro->nombre}}</div>
													<p>
														@php $i=0; @endphp
														@foreach($maestro->clases as $clase)
															@php $i++; @endphp
															{{$clase->nombre}}@if($i == count( $maestro->clases )). @else,&nbsp;@endif
														@endforeach
													</p>
												</div>
											</div>
											@endif
										@endforeach	
									</div>
									
									
									<div class="row">
										
										<h2 style="margin-top: 50px;">CDMX Bosque Real</h2>
										@foreach($maestros as $maestro)
											@if($maestro->sucursal->nombre == "CDMX Bosque Real")
											@php $maestro_num++; @endphp
											<div class="col-sm-3">
												<div class="directivo-elemento">
													<div class="maestro_foto" style="background-image: url('{{asset("public/storage/maestros")}}/{{$maestro->foto}}')"> </div>
													<div class="photo-title">{{$maestro->nombre}}</div>
													<p>
														@php $i=0; @endphp
														@foreach($maestro->clases as $clase)
															@php $i++; @endphp
															{{$clase->nombre}}@if($i == count( $maestro->clases )). @else,&nbsp;@endif
														@endforeach
													</p>
												</div>
											</div>
											@endif
										@endforeach
										
									</div>
								</div>
						    </div>

					    </div>
					    <div class="caja">
						     <div class="directivo">
								<div class="container">
									<div class="row">
										
										@foreach($maestros as $maestro)
											@if($maestro->sucursal->nombre == "Mérida")
											@php $maestro_num++; @endphp
											
											<div class="col-sm-3">
												<div class="directivo-elemento" data-toggle="modal" data-target="#maestro_{{$maestro_num}}">
													<div class="maestro_foto" style="background-image: url('{{asset("public/storage/maestros")}}/{{$maestro->foto}}')"> </div>
													<div class="photo-title">{{$maestro->nombre}}</div>
													<p>
														@php $i=0; @endphp
														@foreach($maestro->clases as $clase)
															@php $i++; @endphp
															{{$clase->nombre}}@if($i == count( $maestro->clases )). @else,&nbsp;@endif
														@endforeach
													</p>
												</div>
											</div>
											
											<div class="modal fade" id="maestro_{{$maestro_num}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
												<div class="modal-dialog" role="document">
													<div class="modal-content">
													
														<div class="modal-header">
															<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
															<h4 class="modal-title" id="myModalLabel">{{$maestro->nombre}}</h4>
														</div>
													
														<div class="modal-body">
														{!!$maestro->descripcion!!}
														</div>
													
														<div class="modal-footer">
															<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
														</div>
													
													</div>
												</div>
											</div>
											
											
											
											@endif
										@endforeach
									</div>
								</div>
						    </div>
					    </div>
				    </div>
			    
				</div>
	
		    </div>
	    </div>

@endsection

@section('js')

	<script>
		
		$(document).ready(function(){
			botones("#maestros");
		});
		
		function botones($elem){
			//Corregir clase activa
			$($elem + " .buttons > .button").removeClass('active');
			$($elem + " .buttons > .button").first().addClass('active');
			
			//Corregir cajas visibles
			$($elem + " .cajas > .caja").hide();
			$($elem + " .cajas > .caja").first().show();
			
			$(document).on('click', $elem + ' .buttons > .button', function(){	
				if(!$(this).hasClass('active')){
					//obtener el índice del hijo 			
					hijo = $($elem + " .buttons > .button").index(this);
					//ocultar cajas
					$($elem + " .cajas .caja").hide();
					//mostrar caja correspondiente al índice
					$($($elem + " .cajas .caja")[hijo]).fadeIn();
					$($elem + ' .buttons > .button').removeClass("active");
					$(this).addClass("active");
				}
			});
		}
		
		
		
	</script>

@endsection