@extends('layouts.layout')

@section('title', 'Contacto | Step Up')


@section('css')
<style>
		.buttons{
			text-align: center;
			margin-bottom: 30px;
		}
		.buttons .button{
			display: inline-block;
			padding: 2px 1px;
			margin: 0px;
			margin: 0px 60px;
			cursor: pointer; 
		}
		.buttons .button.active{
			border-bottom: 2px solid #F58220;
		}
		
		.cajas .caja{
			display: none;
		}
		h2{
			text-transform: uppercase;
			text-align: center;
			margin-bottom: 20px;
			font-weight: 300;
			font-size: 18px;
			font-family: 'Josefin Sans', sans-serif;
		}
		
		
		form{
			text-align: center;
			margin-bottom: 100px;
		}
		.form-input {
		    width: 100%;
		    margin-bottom: 20px;
		    border: 3px solid #F58220;
		    color: #000;
		    padding: 8px 10px;
		    margin-left: auto;
		    margin-right: auto;
		    max-width: 626px;
		}
		textarea.form-input{
			resize: none;
			
		}
		.form_button{
			background-color: #F58220;
			border: 0px solid transparent;
			width: 100%;
			max-width: 626px;
			padding: 13px;
			color: #ffffff;
		}
		
</style>
@endsection

@section('content')
		<section>
		    <div class="banner">
			    <img src="{{asset('images/banners/contacto.png')}}">
		    </div>
	    </section>
	    
	    <div class="seccion">
		    @if($errors->any())
		    <ul>
		    @foreach($errors as $error)
		    	<li>{{$error}}</li>
		    @endforeach
		    </ul>
		    @endif
		    
		    <div class="container">
			    <div class="row">
					<div class="col-sm-12 text-center" style="margin-bottom: 30px"><h1 class="titulo">Contáctanos</h1></div>  
					
					<form action="{{route('contacto_email')}}" method="post" accept-charset="utf-8" id="ajax-contact">
						{{csrf_field()}}
						<div class="row">
							<div class="col-sm-12">
								<input type="text" class="form-input" name="nombre" id="nombre" placeholder="Nombre" required="required" value="{{old('nombre')}}">
							</div>
							<div class="col-sm-12">
								<input type="text" class="form-input" name="email" id="email" placeholder="E-mail" required="required" value="{{old('email')}}">
							</div>
							<div class="col-sm-12">
								<input type="text" class="form-input" name="asunto" id="asunto" placeholder="Asunto" required="required" value="{{old('asunto')}}">
							</div>
							<div class="col-sm-12">
								<select name="sucursal" id="sucursal" placeholder="Sucursal" class="form-input" required="required">
									<option selected value="">Selecciona una Sucursal</option>
									<option value="1">Las Águilas</option>
									<option value="2">Bosque Real</option>
									<option value="3">Mérida</option>
								</select>
							</div>
							
							<div class="col-sm-12">
								<textarea class="form-input" name="mensaje" id="mensaje" rows="7" placeholder="Mensaje" required="required">{{old('mensaje')}}</textarea>
							</div>
							
							<div class="col-sm-12">
								<button class="form_button" type="submit">ENVIAR</button>
							</div>
						</div>
						
					</form>
					
				    
				    <div class="col-sm-12 text-center">
					    <div id="maestros">
							<div class="buttons">
							    <div class="button" 	btn="1">
								    <div class="titulo">CDMX</div>
								</div>
							    <div class="button" 		btn="2">
								    <div class="titulo">MÉRIDA</div>
								</div>
						    </div>

						    
						    
						    
						    <div class="cajas">
							    <div class="caja">
								    
								    <div class="col-sm-12"><h2>Step Up Las Aguilas</h2></div>
								    <p>
									    (554) 921 6762 <br>
									    Calzada de las Águilas 453, Col. Águilas. <br>
									    Entre Dr. Cervantes Ahumada y 2da Cda. Águilas. <br>
									    Del. Álvaro Obregón, Ciudad de México.  <br>
									    E-mail: <a style="text-decoration: none; color: inherit;" href="mailto:contactocdxm@stepup.com.mx">contactocdxm@stepup.com.mx</a> 
								    </p>
								    <div style="margin-bottom: 50px;margin-top: 40px;">
								    	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3764.2143428758054!2d-99.21046798404248!3d19.359869548016494!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d2003e3b84720d%3A0xf86307138a50f4aa!2sStepUp!5e0!3m2!1ses-419!2smx!4v1540351928590" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								    </div>
								    
								    
								    <div class="col-sm-12"><h2>Step Up Bosque Real</h2></div>
								    <p>
									    (555) 159 0334 <br>
									    Carretera México Huixquilican No. 180. <br>
									    Col. Ex ejidos de San Cristobal.<br>
									    Ciudad de México. <br>
									    E-mail: <a style="text-decoration: none; color: inherit;" href="mailto:contactocdxm@stepup.com.mx">contactobr@stepup.com.mx</a> 
								    </p>
								    <div style="margin-top: 40px;">
								    	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2296.368384111848!2d-99.29923848957505!3d19.415704851003177!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d206a302614415%3A0x62554327fdc53568!2sM%C3%A9xico-huixquilucan+180%2C+Bosque+Real%2C+52770+Huixquilucan+de+Degollado%2C+M%C3%A9x.!5e0!3m2!1ses-419!2smx!4v1540352348181" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								    </div>
								    
								    
							    </div>
							    <div class="caja">
								    <div class="col-sm-12"><h2>Step Up Mérida</h2></div>
								    <p>
									    (999) 930 6675<br>
									    C.30 #249 X 73 Y 75 <br>
									    Colonia Montes de Amé <br>
									    Mérida, Yucatán <br>
									    E-mail: <a style="text-decoration: none; color: inherit;" href="mailto:contactomerida@stepup.com.mx">contactomerida@stepup.com.mx</a> 
								    </p>
								    <div style="margin-top: 40px;">
								    	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.021960440086!2d-89.61898188403286!3d21.03180729304786!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f56771c1d183763%3A0xf3834992bb8f6bc7!2sStep+Up+M%C3%A9rida!5e0!3m2!1ses-419!2smx!4v1540352372444" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
								    </div>
							    </div>
						    </div>
					    
						</div>
					    
				    </div>
			    </div>
		    </div>
	    </div>
	    
	    
		
		<div class="modal fade" id="mensajes" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="myModalLabel">Mensaje</h4>
					</div>
					<div class="modal-body">
						
						<p id="mensaje_">
							@if(session("mensaje"))
								{{session("mensaje")}}
							@endif
							@if($errors->any())
							<ul>
								@foreach($errors->all() as $error)
								<li>{{$error}}</li>
								@endforeach
							</ul>
							@endif
							
						</p>
						
						
						
					</div>
					<div class="modal-footer">
						<button type="button" class="button sm" data-dismiss="modal">Ok</button>
					</div>
				</div>
			</div>
		</div>

@endsection

@section('js')
	<script>
		
		$(document).ready(function(){
			botones("#maestros");
		});
		
		function botones($elem){
			//Corregir clase activa
			$($elem + " .buttons > .button").removeClass('active');
			$($elem + " .buttons > .button").first().addClass('active');
			
			//Corregir cajas visibles
			$($elem + " .cajas > .caja").hide();
			$($elem + " .cajas > .caja").first().show();
			
			$(document).on('click', $elem + ' .buttons > .button', function(){	
				if(!$(this).hasClass('active')){
					//obtener el índice del hijo 			
					hijo = $($elem + " .buttons > .button").index(this);
					//ocultar cajas
					$($elem + " .cajas .caja").hide();
					//mostrar caja correspondiente al índice
					$($($elem + " .cajas .caja")[hijo]).fadeIn();
					$($elem + ' .buttons > .button').removeClass("active");
					$(this).addClass("active");
				}
			});
		}
		
		@if(session("mensaje"))
		$("#mensajes").modal('show');
		@endif
		@if($errors->any())
			$("#mensajes").modal('show');
			$("#mensaje_").html('Corrige los siguientes errores:');
		@endif
		
		
		
		$(function() {
		    var form = $('#ajax-contact');
			$(form).submit(function(event) {
			    event.preventDefault();
			    var formData = $(form).serialize();
			    $.ajax({
				    type: 'POST',
				    url: $(form).attr('action'),
				    data: formData
				}).done(function(response) {
					console.log(response);
					$('#mensaje_').html(response);
					$("#mensajes").modal('show');
				    $('#nombre').val('');
				    $('#email').val('');
				    $('#asunto').val('');
				    $('#mensaje').val('');
				}).fail(function(data) {
					$(formMessages).slideDown();
				    if (data !== '') {
				        $(formMessages).text(data);
				    } else {
				        $(formMessages).text('Lo sentimos, tu mensaje no pudo ser enviado. Revisa tu conexión a Internet.');
				    }
				});
			});
		});
		
	</script>
@endsection




	