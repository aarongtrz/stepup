@extends('layouts.layout')


@section('content')
	    
	    <section>
		    <div class="banner">
			    <img src="{{asset('images/banner.png')}}">
		    </div>
	    </section>
	    
	    
	    
	    <section>
		    <div class="intro">
			    <div class="container">
				    <div class="row">
					    <div class="col-sm-6 col-md-5">
						    <h1 class="titulo">¿Quiénes somos?</h1>
						    <p>Step Up nace a partir del sueño de una gran coreógrafa y apasionada del baile; Mariana Esteban, quien después de 10 años de experiencia ha alcanzado un excelente nivel de calidad y creatividad dentro de sus coreografías y quien se ha caracterizado por lograr compartir un poco de su pasión a sus alumnos y lograr que ellos proyecten y disfruten al presentarse en cualquier escenario.</p>
							<p>“Hoy en día Step Up cuenta con tres sucursales - Las Águilas y Bosque Real en Ciudad de México y una en Mérida,Yucatán - en donde gracias a un extraordinario equipo se logra que nuestras Academias sean un increíble espacio para cualquier persona interesada en aprender a bailar.”</p>
					    </div>
				    </div>
			    </div>
			    <div class="bg">
				    <img src="{{asset('images/separador.jpg')}}">
			    </div>
		    </div>
	    </section>
	    
	    
	    <section>
		    <div class="directivo">
				<div class="container">
					<div class="row">
						<div class="col-sm-4">
							<div class="directivo-elemento">
								<img src="{{asset('images/dir/mariana.jpg')}}" class="dir">
								<div class="photo-title">Mariana <br>Esteban Álvarez</div>
								<p>Fundadora y Directora Creativa</p>
							</div>
						</div>
						{{--<div class="col-sm-3">
							<div class="directivo-elemento">
								<img src="{{asset('images/dir/alexa.jpg')}}" class="dir">
								<div class="photo-title">Alexa <br>Zúñiga Rodrigo</div>
								<p>Directora Académica</p>
							</div>
						</div>--}}
						<div class="col-sm-4">
							<div class="directivo-elemento">
								<img src="{{asset('images/dir/cristina.jpg')}}" class="dir">
								<div class="photo-title">Cristina <br>Alonzo Méndez</div>
								<p>Directora Ejecutiva</p>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="directivo-elemento">
								<img src="{{asset('images/dir/lorena.jpg')}}" class="dir">
								<div class="photo-title">Lorena <br>Arroyo</div>
								<p>Coordinadora Suc. Bosque Real</p>
							</div>
						</div>
						
						
					</div>
				</div>
		    </div>
	    </section>
	    
	    
	    <!--<section>
		    <div class="clases">
			    <div class="container">
				    <div class="col-sm-4">
					    <div class="clase">
						    <div class="front"></div>
						    <div class="back"></div>
					    </div>
				    </div>
			    </div>
		    </div>
	    </section>-->
	    
	    
@endsection	   