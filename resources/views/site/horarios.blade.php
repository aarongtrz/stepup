@extends('layouts.layout')

@section('title', 'Horarios | Step Up')


@section('css')
<style>
	.seccion{
		background-color: black;
		border-bottom: 1px solid gray;
	}
	.buttons{
		text-align: center;
	}
	.buttons .button{
		display: inline-block;
		padding: 2px 1px;
		margin: 0px;
		margin: 0px 60px;
		cursor: pointer; 
	}
	.buttons .button.active{
		border-bottom: 2px solid #F58220;
	}
	.cajas{
		font-family: 'Josefin Sans', sans-serif;
	}
	.cajas .caja{
		display: none;
	}
	h2{
		text-transform: uppercase;
		text-align: center;
		margin-bottom: 50px;
		font-weight: 300;
		font-size: 18px;
		color: #fff;
	}
		
		
	.btn-ciudad{
		color: #f58220;
		text-transform: uppercase;
		border: none;
		background-color: transparent;
		font-size: 32px;
		position: relative;
	}
	
	.btn-ciudad.active::after, .btn-ciudad:hover::after{
		content: '';
		position: absolute;
		left: 0;
		bottom: -7px;
		height: 4px;
		background-color: #f58220;
		transition: width .25s;
		width: 100%;
	}
	
	.pestanas{
		margin-bottom: 100px;
	}
	
	.div-btn{
		margin-left: 15px;
		margin-right: 15px;
		display: inline-block;
	}
	
	.tabla .header{
		background-color: #f58220;
		font-weight: 600;
		text-transform: uppercase;
		font-size: 16px;
		border: 3px solid #1a1a1a !important;
		text-align: center;
	}
	
	.tabla tbody td{
		border-top: none !important;
		border-bottom: 1px solid #ddd;
	}
	
	.tabla .hora{
		vertical-align: middle;
		background-color: #1a1a1a;
		color: #999999;
		border: 3px solid #000000;
		width: 85px;
	}
	
	.tabla .nombre-clase{
		text-transform: uppercase;
		color: #fff;
		font-size: 14px;
	}
	
	.tabla .detalle-clase{
		color: #999999;
	}
	
	.tabla .detalle-sala{
		color: #fff;
		text-decoration: underline;
	}
	
	.bg-black{
		background-color: #1a1a1a;
		border: 3px solid #000000 !important;
		
	}
	
	td.bg-black,
	td.hora{
		vertical-align: middle !important;
		text-align: center;
	}
	
	.dnone{
		display: none;
	}
	
	.horarios{
		margin-bottom: 80px;
	}
	/*.horario .col-sm-3{
		padding-right: 0px;
	}*/
	
	.btn-generar{
		background-color: #F58220;
		border: 0px solid transparent;
		padding: 13px;
		color: #000;
		text-transform: uppercase;
		border-radius: 0px;
	}
</style>
@endsection

@section('content')
		<section>
		    <div class="banner">
			    <img src="{{asset('images/banners/horarios.png')}}">
		    </div>
	    </section>
	    
	    <div class="seccion">
		  
	    
			<div class="container">
				
				
				
				<div id="maestros">
						<div class="buttons">
						    <div class="button" 	btn="1">
							    <div class="titulo">CDMX</div>
							</div>
						    <div class="button" 		btn="2">
							    <div class="titulo">MÉRIDA</div>
							</div>
					    </div>
					    
					    
					    
					    <div class="cajas">
						    <div class="caja">
							    
									
								<div class="">
									<h2 style="margin-top: 50px;">CDMX Las Águilas</h2>
									@php $contador = 1; @endphp
									@foreach($sucursales as $sucursal)
									@if($sucursal->nombre=="CDMX Las Aguilas")
									<div class="horario horario-{{ $sucursal->id }} {{ $contador > 1 ? 'dnone' : '' }} horarios" id="horario-{{ $sucursal->id }}">
										<div class="row">
											@foreach($dias as $dia)
											<div class="col-sm-3">
												<div class="tabla">
													<table class="table table-hover">
										                <thead class="">
											                <tr>
												                <th class="header" colspan="2" class="text-center"><b>{{ $dia->nombre }}</b></th>
											                </tr>
										                </thead>
										                @if($dia->horarios->isNotEmpty())
											                @if($dia->horarios->contains('sucursal_id', $sucursal->id))
											                <tbody>
												                @foreach($dia->horarios as $horario)
												                @if($horario->sucursal_id == $sucursal->id)
													                <tr>
														                <td class="hora {{ $sucursal->id }}" rowspan="{{ $horario->clases->count() + 1 }}">
															                <b>{{ substr($horario->hora_inicial, 0, -3) }} </b>
															                a
															                <b>{{ substr($horario->hora_final, 0, -3) }}</b>
														                </td>
													                </tr>
													                @foreach($horario->clases as $clase)
													                <tr>
														                <td class="bg-black">
															                <span class="nombre-clase"><b>{{ $clase->nombre }}</b></span>
															                <br>
															                @if( $clase->detalle!="")
															                <span class="detalle-clase">{{ $clase->detalle }}</span>
															                <br>
															                @endif
															                <span class="detalle-sala">
															                	@if($clase->pivot->sala != "") 
															                		S{{ $clase->pivot->sala }}
															                	@else
															                		{{ $clase->pivot->sala }}
															                	@endif
															                </span>
															               
														                </td>
													                </tr>
													                @endforeach
													            @endif
												                @endforeach
											                </tbody>
											                @endif
										                @endif
										            </table>
												</div>
											</div>
											@endforeach
										</div>
									</div>
									@php $contador++; @endphp
									@endif
									@endforeach
									
								</div>
								
								
								<div class="">
									
									<h2 style="margin-top: 50px;">CDMX Bosque Real</h2>
									@php $contador = 1; @endphp
									@foreach($sucursales as $sucursal)
									@if($sucursal->nombre=="CDMX Bosque Real")
									<div class="horario horario-{{ $sucursal->id }} {{ $contador > 1 ? 'dnone' : '' }} horarios" id="horario-{{ $sucursal->id }}">
										<div class="row">
											@foreach($dias as $dia)
											<div class="col-sm-3">
												<div class="tabla">
													 <table class="table table-hover">
										                <thead class="">
											                <tr>
												                <th class="header" colspan="2" class="text-center"><b>{{ $dia->nombre }}</b></th>
											                </tr>
										                </thead>
										                @if($dia->horarios->isNotEmpty())
											                @if($dia->horarios->contains('sucursal_id', $sucursal->id))
											                <tbody>
												                @foreach($dia->horarios as $horario)
												                @if($horario->sucursal_id == $sucursal->id)
													                <tr>
														                <td class="hora {{ $sucursal->id }}" rowspan="{{ $horario->clases->count() + 1 }}">
															                <b>{{ substr($horario->hora_inicial, 0, -3) }} </b>
															                a
															                <b>{{ substr($horario->hora_final, 0, -3) }}</b>
														                </td>
													                </tr>
													                @foreach($horario->clases as $clase)
													                <tr>
														                <td class="bg-black">
															                <span class="nombre-clase"><b>{{ $clase->nombre }}</b></span>
															                <br>
															                @if( $clase->detalle!="")
															                <span class="detalle-clase">{{ $clase->detalle }}</span>
															                <br>
															                @endif
															                <span class="detalle-sala">
															                	@if($clase->pivot->sala != "") 
															                		S{{ $clase->pivot->sala }}
															                	@else
															                		{{ $clase->pivot->sala }}
															                	@endif
															                </span>
															               
														                </td>
													                </tr>
													                @endforeach
													            @endif
												                @endforeach
											                </tbody>
											                @endif
										                @endif
										            </table>
												</div>
											</div>
											@endforeach
										</div>
									</div>
									@php $contador++; @endphp
									@endif
									@endforeach
									
								</div>
									
							    
	
						    </div>
						    <div class="caja">
							     
									
								<div style="margin-top: 50px;">
									@php $contador = 1; @endphp
									@foreach($sucursales as $sucursal)
									@if($sucursal->nombre=="Mérida")
									<div class="horario horario-{{ $sucursal->id }} {{ $contador > 1 ? 'dnone' : '' }} horarios" id="horario-{{ $sucursal->id }}">
										<div class="row">
											@foreach($dias as $dia)
											<div class="col-sm-3">
												<div class="tabla">
													 <table class="table table-hover">
										                <thead class="">
											                <tr>
												                <th class="header" colspan="2" class="text-center"><b>{{ $dia->nombre }}</b></th>
											                </tr>
										                </thead>
										                @if($dia->horarios->isNotEmpty())
											                @if($dia->horarios->contains('sucursal_id', $sucursal->id))
											                <tbody>
												                @foreach($dia->horarios as $horario)
												                @if($horario->sucursal_id == $sucursal->id)
													                <tr>
														                <td class="hora {{ $sucursal->id }}" rowspan="{{ $horario->clases->count() + 1 }}">
															                De <b>{{ substr($horario->hora_inicial, 0, -3) }} </b>
															                a
															                <b>{{ substr($horario->hora_final, 0, -3) }}</b>
														                </td>
													                </tr>
													                @foreach($horario->clases as $clase)
													                <tr>
														                <td class="bg-black">
															                <span class="nombre-clase"><b>{{ $clase->nombre }}</b></span>
															                <br>
															                @if( $clase->detalle!="")
															                <span class="detalle-clase">{{ $clase->detalle }}</span>
															                <br>
															                @endif
															                <span class="detalle-sala">
															                	@if($clase->pivot->sala != "") 
															                		S{{ $clase->pivot->sala }}
															                	@else
															                		{{ $clase->pivot->sala }}
															                	@endif
															                </span>
															               
														                </td>
													                </tr>
													                @endforeach
													            @endif
												                @endforeach
											                </tbody>
											                @endif
										                @endif
										            </table>
												</div>
											</div>
											@endforeach
										</div>
									</div>
									@php $contador++; @endphp
									@endif
									@endforeach
									
								</div>
									
							    
						    </div>
					    </div>
				    
					</div>
				
				
			</div>
		
		
		</div>
		
		
@endsection

@section('js')
<script>
	$(document).ready(function(){
		botones("#maestros");
	});
	
	function botones($elem){
		//Corregir clase activa
		$($elem + " .buttons > .button").removeClass('active');
		$($elem + " .buttons > .button").first().addClass('active');
		
		//Corregir cajas visibles
		$($elem + " .cajas > .caja").hide();
		$($elem + " .cajas > .caja").first().show();
		
		$(document).on('click', $elem + ' .buttons > .button', function(){	
			if(!$(this).hasClass('active')){
				//obtener el índice del hijo 			
				hijo = $($elem + " .buttons > .button").index(this);
				//ocultar cajas
				$($elem + " .cajas .caja").hide();
				//mostrar caja correspondiente al índice
				$($($elem + " .cajas .caja")[hijo]).fadeIn();
				$($elem + ' .buttons > .button').removeClass("active");
				$(this).addClass("active");
			}
		});
	}
</script>
@endsection