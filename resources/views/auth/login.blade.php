@extends('layouts.auth.template')

@section('titulo')
Iniciar Sesión
@endsection

@section('contenido')
<form class="login100-form validate-form" method="post" action="{{ route('login') }}">
	{{ csrf_field() }}
	<span class="login100-form-logo">
		<i class="far fa-user" style="color: #f58220;"></i>
	</span>

	<span class="login100-form-title p-b-34 p-t-27">
		Iniciar Sesión
	</span>

	<div class="wrap-input100 validate-input" data-validate = "Ingrese correo electrónico">
		<input required="required" class="input100" type="email" name="email" placeholder="Correo Electrónico">
		<span class="focus-input100 mail"></span>
	</div>

	<div class="wrap-input100 validate-input" data-validate="Ingrese contraseña">
		<input required="required" class="input100" type="password" name="password" placeholder="Contraseña">
		<span class="focus-input100 password"></span>
	</div>

	<div class="contact100-form-checkbox">
		<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
		<label class="label-checkbox100" for="ckb1">
			Recuérdame
		</label>
	</div>

	<div class="container-login100-form-btn">
		<button class="login100-form-btn" type="submit">
			Ingresar
		</button>
	</div>

	{{-- <div class="text-center p-t-90">
		<a class="txt1" href="#">
			¿Olvidaste tu contraseña?
		</a>
	</div> --}}
</form>
@endsection