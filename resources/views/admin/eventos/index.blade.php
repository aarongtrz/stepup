@extends('layouts.admin.template')

@section('titulo')
Eventos
@endsection

@section('titulo_pagina')
Eventos
@endsection

@section('active_eventos')
active
@endsection

@section('content')
<div class="card">
    <div class="card-header card-header-warning">
        <h4 class="card-title mt-0"> Lista de Eventos</h4>
        <p class="card-category"> Eventos registrados en el sistema</p>
    </div>
    <div class="card-body">
	    <div class="row">
		    <div class="col-sm-12 text-right">
			    <a href="{{ route('admin.eventos.edit') }}" class="btn btn-default"><i class="material-icons">add</i> Agregar Nuevo Evento</a>
		    </div>
	    </div>
	    @if($eventos->isNotEmpty())
	    <div class="row">
		    <div class="col-sm-12">
		        <div class="table-responsive">
		            <table class="table table-hover">
		                <thead class="">
		                    <tr>
		                        <th>
		                            Título
		                        </th>
		                        <th>
		                            Descripción
		                        </th>
		                        <th>
			                        Acciones
		                        </th>
		                    </tr>
		                </thead>
		                <tbody>
		                    @foreach($eventos as $evento)
		                    <tr>
			                    <td>
				                    {{ $evento->titulo }}
			                    </td>
			                    <td>
				                    {{ strlen(strip_tags($evento->descripcion)) > 150 ? substr(strip_tags($evento->descripcion), 0, 150) : strip_tags($evento->descripcion) }}
			                    </td>
			                    <td>
				                    <a href="{{ route('admin.eventos.edit', ['id' => encrypt($evento->id)]) }}" class="btn btn-warning btn-round btn-fab">
						            	<i class="material-icons">edit</i>
										<div class="ripple-container"></div>
						            </a>
						            <button type="button" class="btn btn-danger btn-round btn-fab eliminar" data-id="{{ encrypt($evento->id) }}">
						            	<i class="material-icons">delete</i>
										<div class="ripple-container"></div>
						            </button>
			                    </td>
		                    </tr>
		                    @endforeach
		                </tbody>
		            </table>
		        </div>
		    </div>
	    </div>
	    <div class="row">
		    <div class="col-sm-12">
			    <div class="paginacion text-center">
			    	{{ $eventos->links() }}
			    </div>
		    </div>
	    </div>
        @else
        <h5>No hay registros todavía...</h5>
        @endif
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		var rutaEliminar = "{{ route('admin.eventos.delete') }}";
		
		function eliminar(id){
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
		    	type: 'POST',
				url: rutaEliminar,
				data: {id:  id, _method: 'delete' },
				dataType: 'json',
				beforeSend: function(){
			    }
			}).done(function(response) {
				var estado = response.estado;
				if(estado == 0){
					alertSuccess(response.mensaje, true);
				}
				if(estado == 1){
					alertError(response.mensaje);
				}
		    }).fail(function(data) {
			    var errors = data.responseJSON;
			    var html = '';
			    if(errors instanceof Array){
				    $.each( errors, function( key, value ) {
						for(var i = 0; i < value.length; i++){
							html += '<li>'+ value[i] +'</li>';
						}
		            });
			    }else{
				    html = data.responseJSON;
			    }
			    alertError(html);
		    });
		}
	</script>
@endsection