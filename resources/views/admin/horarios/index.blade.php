@extends('layouts.admin.template')

@section('titulo')
Horarios
@endsection

@section('titulo_pagina')
Horarios
@endsection

@section('active_horarios')
active
@endsection

@section('content')
	<div class="row">
		<div class="col-sm-12 text-center">
			<a class="btn btn-generar" href="{{ route('descargaPdf') }}">Descargar Horario en PDF</a>
		</div>
	</div>
	@foreach($sucursales as $sucursal)
	<div class="separador">
		<div class="card">
		    <div class="card-header card-header-warning">
		        <h4 class="card-title mt-0"> Horario Sucursal {{ $sucursal->nombre }}</h4>
		        <p class="card-category"> Horarios de Clases</p>
		    </div>
		    <div class="card-body">
			    @if($dias->isNotEmpty())
			    <div class="row">
				    @foreach($dias as $dia)
					    <div class="col-sm-3">
						    <div class="table-responsive">
					            <table class="table table-hover">
					                <thead class="">
						                <tr>
							                <th colspan="2" class="text-center"><b>{{ $dia->nombre }}</b></th>
						                </tr>
					                </thead>
					                @if($dia->horarios->isNotEmpty())
						                @if($dia->horarios->contains('sucursal_id', $sucursal->id))
						                <tbody>
							                @foreach($dia->horarios as $horario)
							                @if($horario->sucursal_id == $sucursal->id)
								                <tr>
									                <td rowspan="{{ $horario->clases->count() + 1 }}">
										                De <b>{{ $horario->hora_inicial }} </b>
										                <br>a<br>
										                <b>{{ $horario->hora_final }}</b>
									                </td>
								                </tr>
								                @foreach($horario->clases as $clase)
								                <tr>
									                <td>
										                <b>{{ $clase->nombre }}</b>
										                <br>
										                {{ $clase->detalle }}
										                <br>
										                S{{ $clase->pivot->sala }}
										                <br>
										                <button style="padding: 5px;" class="btn btn-danger btn-sm eliminar" type="button" data-id="{{ encrypt($horario->id) }}">Eliminar</button>
									                </td>
								                </tr>
								                @endforeach
								            @endif
							                @endforeach
						                </tbody>
						                @endif
					                @endif
					                <tfoot>
						                <tr>
							                <td colspan="2" class="text-center">
								                <button type="button" class="btn btn-warning btn-round btn-fab agregarHorario" data-id="{{ encrypt($dia->id) }}" data-dia="{{ $dia->nombre }}" data-sucursal="{{ $sucursal->nombre }}" data-sucursalid="{{ encrypt($sucursal->id) }}">
									            	<i class="material-icons">add</i>
													<div class="ripple-container"></div>
									            </button>
							                </td>
						                </tr>
					                </tfoot>
					            </table>
						    </div>
					    </div>
				    @endforeach
			    </div>
		        @else
		        <h5>No hay registros todavía...</h5>
		        @endif
		    </div>
		</div>
	</div>
	@endforeach
	<script id="template" type="x-tmpl-mustache">
		<div class="container-template" id="template-@{{ num_template }}">
			<div class="row">
				<div class="col-sm-4">
					<div class="form-group">
					    <label for="clases" class="bmd-label-floating">Selecciona la clase</label>
					    <select class="form-control" name="clases[]" id="clases">
						    <option value="">Selecciona una opción</option>
							@foreach($clases as $clase)
							<option value="{{ encrypt($clase->id) }}">{{ $clase->nombre }} ({{ $clase->detalle }})</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-sm-4">
	                <div class="form-group" style="margin-top: 13px;">
					    <label for="num_sala" class="bmd-label-floating">Número de Sala (opcional)</label>
					    <input  type="text" class="form-control" id="num_sala" name="salas[]" value="">
					</div>
				</div>
				<div class="col-sm-4">
					<br>
					<button type="button" class="btn btn-warning btn-round btn-fab eliminarTemplate" data-template="@{{ num_template }}">
		            	<i class="material-icons">clear</i>
						<div class="ripple-container"></div>
		            </button>
				</div>
			</div>
		</div>
	</script>
@endsection

@section('modals')
<div id="modalHorario" class="modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
	        <form method="post" action="{{ route('admin.horarios.save') }}" id="form-horario">
		        <input type="hidden" id="dia_id" name="dia_id">
		        <input type="hidden" id="sucursal_id" name="sucursal_id">
		        <input type="hidden" id="token_form" name="_token">
	            <div class="modal-header">
	                <h5 class="modal-title">Nuevo Horario (Sucursal: <span id="sucursal_nombre"></span>. Día: <span id="dia"></span>)</h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	                <div class="row">
		                <div class="col-sm-6">
			                <div class="form-group">
							    <label for="hora_inicial" class="bmd-label-floating">Hora Inicial</label>
							    <input required="required" type="text" class="form-control reloj" id="hora_inicial" name="hora_inicial" value="" data-align="top" data-autoclose="true">
							</div>
		                </div>
		                <div class="col-sm-6">
			                <div class="form-group">
							    <label for="hora_final" class="bmd-label-floating">Hora Final</label>
							    <input required="required" type="text" class="form-control reloj" id="hora_final" name="hora_final" value="" data-align="top" data-autoclose="true">
							</div>
		                </div>
	                </div>
	                <br>
	                <div class="row">
		                <div class="col-sm-12 text-right">
			                <button type="button" class="btn btn-warning agregarClase">Agregar Clase</button>
		                </div>
	                </div>
	                <div id="container-target">
		                
	                </div>
	                <br><br>
	            </div>
	            <div class="modal-footer">
	                <button type="submit" class="btn btn-warning">Guardar</button>
	                <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	            </div>
	        </form>
        </div>
    </div>
</div>
@endsection

@section('css')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.css" />
<style>
	.separador{
		margin-bottom: 150px;
	}
	
	tfoot{
		border-top: 1px solid rgba(0, 0, 0, 0.06);
	}
	
	span.clockpicker-span-hours.text-primary, span.clockpicker-span-minutes.text-primary{
		color: #f58220 !important;
	}
</style>
@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/clockpicker/0.0.7/bootstrap-clockpicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/mustache.js/3.0.0/mustache.min.js"></script>
	<script type="text/javascript">
		var rutaEliminar = "{{ route('admin.horarios.delete') }}";
		
		$(document).ready(function(){
			empezarEventos();
			token();
		});
		
		function eliminar(id){
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
		    	type: 'POST',
				url: rutaEliminar,
				data: {id:  id, _method: 'delete' },
				dataType: 'json',
				beforeSend: function(){
			    }
			}).done(function(response) {
				var estado = response.estado;
				if(estado == 0){
					alertSuccess(response.mensaje, true);
				}
				if(estado == 1){
					alertError(response.mensaje);
				}
		    }).fail(function(data) {
			    var errors = data.responseJSON;
			    var html = '';
			    if(errors instanceof Array){
				    $.each( errors, function( key, value ) {
						for(var i = 0; i < value.length; i++){
							html += '<li>'+ value[i] +'</li>';
						}
		            });
			    }else{
				    html = data.responseJSON;
			    }
			    alertError(html);
		    });
		}
		
		function empezarEventos(){
			$(document).on('click', '.agregarHorario', function(e){
				$('#form-horario input').val('');
				$('#dia_id').val($(this).data('id'));
				$('#dia').html($(this).data('dia'));
				$('#sucursal_nombre').html($(this).data('sucursal'));
				$('#sucursal_id').val($(this).data('sucursalid'));
				$('#modalHorario').modal('show');
				token();
			});
			
			$(document).on('click', '.eliminarTemplate', function(e){
				var num = $(this).data('template');
				
				$('#template-'+num).remove();
			});
			
			$(document).on('click', '.agregarClase', function(e){
				console.log('hola');
				insertaTemplate();
			});
			
			$('.reloj').on('keydown keyup', function(e){
		        e.preventDefault();
	        });
			
			$('.reloj').clockpicker();
		}
		
		function insertaTemplate(){
			var num_templates = $('.container-template').length;
			var template = $('#template').html();
			Mustache.parse(template);   // optional, speeds up future uses
			var rendered = Mustache.render(template, 
			{
				num_template: num_templates
			});
			$('#container-target').append(rendered);
		}
		
		function token(){
			$('#token_form').val($('meta[name="csrf-token"]').attr('content'));
		}
	</script>
@endsection