@extends('layouts.admin.template')

@section('titulo')
Maestros
@endsection

@section('titulo_pagina')
Maestros
@endsection

@section('active_maestros')
active
@endsection

@section('content')
<div class="row">
	<div class="col-sm-12">
		<a href="{{ route('admin.maestros.index') }}" class="btn btn-default"><i class="material-icons">chevron_left</i> Listado de Maestros</a>
	</div>
</div>
<div class="card">
    <div class="card-header card-header-warning">
        <h3 class="card-title">Agregar/Editar Maestro</h3>
        <p class="card-category">Formulario para maestros</p>
    </div>
    <div class="card-body">
        <form method="post" action="{{ route('admin.maestros.save') }}" enctype="multipart/form-data">
	        {{ csrf_field() }}
	        {{ $maestro ? method_field('PUT') : '' }}
	        <input type="hidden" name="id" value="{{ $maestro ? encrypt($maestro->id) : '' }}">
	        <div class="row">
		        <div class="col-sm-12 text-center">
				    <div class="form-group">
					    <label>Foto*</label>
					    <div class="preview-container">
                        	<div class="preview-img" id="preview" style="background-image: url('{{ $maestro ? asset("public/storage/maestros/".$maestro->foto) : asset("public/images/admin/misc/img-placeholder.png") }}')">
						    
					    	</div>
					    </div>
					    <div>
					    	<small>(Tamaño recomendado 1500x700. La imagen no puede pesar más de 5 MB)</small>
				    	</div>
					    <label style="color: white;" for="foto" class="btn btn-warning">Clic aquí para cambiar la imagen</label>
					    <input accept=".png, .jpg, .jpeg" type="file" class="form-control imagenes" name="foto" id="foto" value="{{ old('foto') ? old('foto') : '' }}">
				    </div>
			    </div>
	        </div>
            <div class="row">
	            <div class="col-sm-6">
		            <br>
		            <div class="form-group">
					    <label for="nombre" class="bmd-label-floating">Nombre</label>
					    <input required="required" type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre') ? old('nombre') : ($maestro ? $maestro->nombre : '') }}">
					</div>
	            </div>
	            <div class="col-sm-6">
		            <div class="form-group">
					    <label for="clases" class="bmd-label-floating">Selecciona la(s) clase(s)</label>
					    <select class="form-control" name="clases[]" multiple="multiple" id="clases">
							@foreach($clases as $clase)
							<option value="{{ encrypt($clase->id) }}" {{ $maestro ? ($maestro->clases->contains($clase->id) ? 'selected=selected' : '') : '' }}>{{ $clase->nombre }}</option>
							@endforeach
						</select>
					</div>
	            </div>
            </div>
            <div class="row">
	            <div class="col-sm-6">
		            <div class="form-group">
			            <label for="sucursal">Sucursal</label>
					    <select required="required" class="form-control" id="sucursal" name="sucursal">
					    	<option value="">Selecciona una sucursal</option>
					    	@foreach($sucursales as $sucursal)
					    	<option value="{{ encrypt($sucursal->id) }}" {{ $maestro ? ($maestro->sucursal_id == $sucursal->id ? 'selected=selected' : '') : '' }}>{{ $sucursal->nombre }}</option>
					    	@endforeach
					    </select>
		            </div>
	            </div>
            </div>
            
            <div class="row">
	            <div class="col-sm-12">
		            <div class="form-group">
			            <label for="descripcion">Semblanza o descripción.</label>
					    <textarea id="descripcion" name="descripcion"></textarea>
		            </div>
	            </div>
            </div>
            
            <div class="row">
	            <div class="col-sm-12 text-right">
		            <button type="submit" href="{{ route('admin.clases.index') }}" class="btn btn-warning"><i class="material-icons">check</i> Guardar</a>
	            </div>
            </div>
        </form>
    </div>
</div>
<div id="summernoteMark" style="display: none;">{!! $maestro ? $maestro->descripcion : '' !!}</div>

@endsection

@section('css')
<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css" rel="stylesheet" />
<style>
	.preview-container{
		width: 150px;
		height: 150px;
		margin: 0 auto;
	}
	
	.preview-img{
		width: 150px;
		height: 150px;
		background-position: center center;
		background-size: 100% auto;
		background-repeat: no-repeat;
		border-radius: 50%;
	}
	
	.fancybox-preview{
		text-align: center;
	}
	
	.imagenes{
		display: none;
	}
	
	.container-template{
		margin-bottom: 50px;
	}
	
	.note-toolbar-wrapper{
		background-color: #f5f5f5;
	}
	
	input.note-image-input.note-form-control.note-input{
		z-index: 100;
		opacity: 1;
		position: relative;
	}
	.note-editor.note-frame .note-editing-area .note-editable{
		min-height: 300px;
	}
</style>

@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-bs4.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.10/lang/summernote-es-ES.js"></script>
<script>
	$(document).ready(function() {
	    clasesSelect();
	    iniciarImg();
		comprobarImg();
		iniciarSummernote();
	});
	
	function clasesSelect(){
		$('#clases').select2({
			placeholder: ''
		});
	}
	
	function hasExtension(inputID, exts){
		var fileName = document.getElementById(inputID).value;
		return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$', "i")).test(fileName);
	}
	
	function readURL(input) {
		var id = $(input).attr('id');
		if(input.files && input.files[0]) {
			var reader = new FileReader();
			
			if(hasExtension(id, ['.jpg', '.png', 'jpeg'])){
				reader.onload = function(e) {
			    	$('#preview').css('background-image', 'url(' + e.target.result + ')');
			    }
				reader.readAsDataURL(input.files[0]);
			}else{
				alertError('El formato de la imagen no es un formato válido');
			}
		}
	}
	
	function iniciarImg(){
		$(document).on('change', '.imagenes', function() {
			readURL(this);
		});
	}
	
	function comprobarImg(){
		if($('.imagenes').length > 0){
			$('.imagenes').each(function(i, elem){
				if ($(elem).get(0).files.length > 0) {
					readURL(elem);
				}
			});
		}
		
	}
	function iniciarSummernote(){
		$('#descripcion').summernote({
			lang: 'es-ES'
		});
		
		$('.btn-primary').removeClass('btn-primary').addClass('btn-warning');
		$('#descripcion').summernote('code', $('#summernoteMark').html());
	}
</script>
@endsection