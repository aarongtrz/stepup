@extends('layouts.admin.template')

@section('titulo')
Clases
@endsection

@section('titulo_pagina')
Clases
@endsection

@section('active_clases')
active
@endsection

@section('content')
<div class="card">
    <div class="card-header card-header-warning">
        <h4 class="card-title mt-0"> Lista de Clases</h4>
        <p class="card-category"> Clases registradas en el sistema</p>
    </div>
    <div class="card-body">
	    <div class="row">
		    <div class="col-sm-12 text-right">
			    <a href="{{ route('admin.clases.edit') }}" class="btn btn-default"><i class="material-icons">add</i> Agregar Nueva Clase</a>
		    </div>
	    </div>
	    @if($clases->isNotEmpty())
	    <div class="row">
		    <div class="col-sm-12">
		        <div class="table-responsive">
		            <table class="table table-hover">
		                <thead class="">
		                    <tr>
		                        <th>
		                            Nombre
		                        </th>
		                        <th>
		                            Detalle
		                        </th>
		                        <th>
		                            Acciones
		                        </th>
		                    </tr>
		                </thead>
		                <tbody>
		                    @foreach($clases as $clase)
		                    <tr>
			                    <td>
				                    <b>{{ $clase->nombre }}</b> 
			                    </td>
			                    <td>
				                    {{ $clase->detalle }}
			                    </td>
			                    <td>
				                    <a href="{{ route('admin.clases.edit', ['id' => encrypt($clase->id)]) }}" class="btn btn-warning btn-round btn-fab">
						            	<i class="material-icons">edit</i>
										<div class="ripple-container"></div>
						            </a>
						            <button type="button" class="btn btn-danger btn-round btn-fab eliminar" data-id="{{ encrypt($clase->id) }}">
						            	<i class="material-icons">delete</i>
										<div class="ripple-container"></div>
						            </button>
			                    </td>
		                    </tr>
		                    @endforeach
		                </tbody>
		            </table>
		        </div>
		    </div>
	    </div>
	    <div class="row">
		    <div class="col-sm-12">
			    <div class="paginacion text-center">
			    	{{ $clases->links() }}
			    </div>
		    </div>
	    </div>
        @else
        <h5>No hay registros todavía...</h5>
        @endif
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		var rutaEliminar = "{{ route('admin.clases.delete') }}";
		
		function eliminar(id){
			$.ajax({
				headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    },
		    	type: 'POST',
				url: rutaEliminar,
				data: {id:  id, _method: 'delete' },
				dataType: 'json',
				beforeSend: function(){
			    }
			}).done(function(response) {
				var estado = response.estado;
				if(estado == 0){
					alertSuccess(response.mensaje, true);
				}
				if(estado == 1){
					alertError(response.mensaje);
				}
		    }).fail(function(data) {
			    var errors = data.responseJSON;
			    var html = '';
			    if(errors instanceof Array){
				    $.each( errors, function( key, value ) {
						for(var i = 0; i < value.length; i++){
							html += '<li>'+ value[i] +'</li>';
						}
		            });
			    }else{
				    html = data.responseJSON;
			    }
			    alertError(html);
		    });
		}
	</script>
@endsection