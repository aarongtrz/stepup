<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class HorariosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "hora_inicial"		=>			"required|date_format:H:i|max:191",
            "hora_final"		=>			"required|date_format:H:i|max:191",
            "dia_id"			=>			"required",
            "sucursal_id"		=>			"required",
            "clases"			=>			"required"
        ];
    }
}
