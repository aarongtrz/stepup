<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class MaestrosRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre"			=>			"required|string|max:191",
            "foto"				=>			"nullable|mimes:jpeg,png,jpg|max:5120",
            "sucursal"			=>			"required",
            "clases"			=>			"required",
            "descripcion"		=>			""
        ];
    }
}
