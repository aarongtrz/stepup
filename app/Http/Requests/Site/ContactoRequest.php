<?php

namespace App\Http\Requests\Site;

use Illuminate\Foundation\Http\FormRequest;

class ContactoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "nombre"		=>	"required|string",
            "email"			=>	"required|email",
            "asunto" 		=> 	"required",
            "sucursal" 		=> 	"required",
            "mensaje" 		=> 	"required"
        ];
    }
}
