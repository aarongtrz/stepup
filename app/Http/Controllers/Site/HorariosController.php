<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Horario;
use App\Models\Dia;
use App\Models\Sucursal;

class HorariosController extends Controller
{
    public function index(){
	    $dias = Dia::with(['horarios' => function($query){
		    $query->orderBy('hora_inicial', 'asc');
	    }, 'horarios.clases'])->get();
	    
	    $sucursales = Sucursal::all();
	    
	    return view('site.horarios')->with(compact('dias', 'sucursales'));
    }
}
