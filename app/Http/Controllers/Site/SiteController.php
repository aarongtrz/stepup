<?php

namespace App\Http\Controllers\Site;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Clase;
use App\Models\Dia;
use App\Models\Evento;
use App\Models\Horario;
use App\Models\Maestro;
use App\Models\Sucursal;

use App\Http\Requests\Site\ContactoRequest;

use DB;
use PDF;
use App;
use View;
use Flickr;

class SiteController extends Controller
{
	
	public function landing(){
		return view('site.landing');
	}
    public function maestros(){
	    $maestros = Maestro::with('Sucursal')->with('clases')->get();
	    return view('site.maestros', compact('maestros'));
    }
    
    public function galeria(Request $request){
	    $photos_per_page = 20;
	    $photo_page = $request->photo_page;
	    if(empty($photo_page)){
		    $photo_page = 1;
	    }
	    $parameters = ['gallery_id' => '66911286-72157699580361272', 'per_page' => $photos_per_page, 'page' => $photo_page, 'continuation' => 1, 'extras' => 'media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'];
	    $method = 'flickr.galleries.getPhotos';
	    $flickr_imagenes = Flickr::request($method, $parameters);
	    
	    //Album Sucursal Aguilas
	    /*$parameters = ['photoset_id' => '72157703519551835', 'user_id' => '160403126@N03', 'extras' => 'media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'];
	    $method = 'flickr.photosets.getPhotos';
	    $flickr_aguilas =  Flickr::request($method, $parameters);

	    $links = $this->paginacionFotos($flickr_imagenes->photos ? $flickr_imagenes->photos['total'] : 1, $photos_per_page, $photo_page);*/

	    
	    //167981105-72157703416799464 VIDEOS
	    /*$parameters = ['gallery_id' => '167981105-72157675573970938', 'continuation' => 1, 'extras' => 'media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'];
	    $method = 'flickr.galleries.getPhotos';
	    $flickr_videos = Flickr::request($method, $parameters);*/
	    
	    $parameters = ['photoset_id' => '72157703591638695', 'user_id' => '160403126@N03', 'extras' => 'media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'];
	    $method = 'flickr.photosets.getPhotos';
	    $flickr_videos =  Flickr::request($method, $parameters);
	    
	    
	    $videos = array();
	    if($flickr_videos->stat == 'ok'){
		    if(!empty($flickr_videos->photoset['photo'])){
			    foreach($flickr_videos->photoset['photo'] as $photo){
				    $foto_id = $photo['id'];
				    $parameters_size = ['photo_id' => $foto_id];
				    
				    $method_sizes = 'flickr.photos.getSizes';
				    $sizes_response = Flickr::request($method_sizes, $parameters_size);
				    if($sizes_response->stat == 'ok'){
					    if(isset($sizes_response->sizes['size'][9])){
						    $videos[] = array('width' => $sizes_response->sizes['size'][9]['width'], 'height' => $sizes_response->sizes['size'][9]['height'], 'source' => $sizes_response->sizes['size'][9]['source']);
					    }
				    }
				    
			    }
		    }
	    }
	    
	    
		return view('site.galeria')->with(compact('flickr_imagenes', 'videos', 'links'));    
    }
    
    public function galeriaDetalle(Request $request){
	    $slug = $request->slug;
	    $user_id = '160403126@N03';
	    $album_id = '';
	    $parameters = array();
	    
	    switch($slug){
		    case 'sucursal-aguilas':
		    	$album_id = '72157703519551835';
		    break;
		    case 'sucursal-merida':
		    	$album_id = '72157701874489611';
		    break;
		    case 'bosque-real':
		    	$album_id = '72157702084757921';
		    break;
		    default:
		    		$album_id = '';
	    }
	    
	    $parameters = ['user_id' => $user_id, 'photoset_id' => $album_id, 'extras' => 'media, path_alias, url_sq, url_t, url_s, url_q, url_m, url_n, url_z, url_c, url_l, url_o'];
	    $method = 'flickr.photosets.getPhotos';
	    $flickr_imagenes = Flickr::request($method, $parameters);
	    

	    
	    return view('site.galeria-detalle')->with(compact('flickr_imagenes'));
    }
    
    public function paginacionFotos($total, $limite, $pagina, $links = 7){
	    $list_class = 'pagination paginacion-fotos';
	    
	    $last = ceil($total / $limite);
	    
	    $start      = ( ( $pagina - $links ) > 0 ) ? $pagina - $links : 1;
	    
	    $end        = ( ( $pagina + $links ) < $last ) ? $pagina + $links : $last;
	    
	    $html       = '<ul class="' . $list_class . '">';
	    
	    $class      = ( $pagina == 1 ) ? "disabled" : "";
	    $href		= ( $pagina == 1 ) ? "href='#'" : 'href="'.route('galeria', ['photo_page' => ($pagina + 1)]).'"';
	    $html       .= '<li class="' . $class . '"><a '.$href.'>&laquo;</a></li>';
	    
	    if ( $start > 1 ) {
	        //$html   .= '<li><a href="?photo_page=1">1</a></li>';
	        $html   .= '<li><a href="'.route('galeria', ['photo_page' => 1]).'">1</a></li>';
	        $html   .= '<li class="disabled" disabled="disabled"><span>...</span></li>';
	    }
	    
	    for ( $i = $start ; $i <= $end; $i++ ) {
	        $class  = ( $pagina == $i ) ? "active" : "";
	        $html   .= '<li class="' . $class . '"><a href="'.route('galeria', ['photo_page' => $i]).'">' . $i . '</a></li>';
	    }
	    
	    if ( $end < $last ) {
	        $html   .= '<li class="disabled" disabled="disabled"><span>...</span></li>';
	        $html   .= '<li><a href="'.route('galeria', ['photo_page' => $last]).'">' . $last . '</a></li>';
	    }
	    
	    $class      = ( $pagina == $last ) ? "disabled" : "";
	    $href		= ( $pagina == $last ) ? "href='#'" : 'href="'.route('galeria', ['photo_page' => ($pagina + 1)]).'"';
	    $html       .= '<li class="' . $class . '"><a '.$href.'>&raquo;</a></li>';
	 
	    $html       .= '</ul>';
	 
	    return $html;
	    
    }
    
    public function eventos(){
	    $eventos = Evento::orderBy('id', 'desc')->paginate(8);
		return view('site.eventos', compact('eventos'));	    
    }
    
    public function contacto(){
	    return view('site.contacto');
    }
    
    public function contacto_email(ContactoRequest $request){
	    $nombre 	= $request->nombre;
	    $email 		= $request->email; 
	    $asunto 	= $request->asunto;
	    $sucursal 	= $request->sucursal;
	    $mensaje 	= $request->mensaje;
	    $sucursal_nombre = '';
	    
	    $recipient = '';
	            
        switch($sucursal){
	        case 1:
	        	$sucursal_nombre = 'Las Águilas';
	        	$recipient = 'contactocdxm@stepup.com.mx';
	        break;
	        case 2:
	        	$sucursal_nombre = 'Bosque Real';
	        	$recipient = 'contactobr@stepup.com.mx';
	        break;
	        case 3:
	        	$recipient = 'contactomerida@stepup.com.mx';
	        	$sucursal_nombre = 'Mérida';
	        break;
	        default:
	        	return back()->withErrors('La sucursal no es válida');
        }
        
	
	    // Set the email subject.
	    $subject = utf8_encode("Web Step Up: ".$asunto);
	
	    // Build the email content.
	    $email_content = "Nombre: ". $nombre."\n";
	    $email_content .= "E-mail: ".$email."\n\n";
	    $email_content .= "Sucursal: ".$sucursal_nombre."\n\n";
	    $email_content .= "Mensaje:\n".$mensaje."\n";
	
	    // Build the email headers.
	    $email_headers = "From: ".$nombre." <".$email.">";
	
	    // Send the email.
	    if (mail($recipient, $subject, utf8_decode($email_content), $email_headers)) {
	        return '¡Gracias! Tu mensaje ha sido enviado. Te contactaremos pronto. ';
	        //return redirect()->back()->with('mensaje', "¡Gracias! Tu mensaje ha sido enviado. Te contactaremos pronto. ") ;
	    } else {
	        return '¡Lo sentimos! Tu mensaje no pudo ser enviado. Revisa que la información sea correcta.';
	        //return redirect()->back()->with('mensaje',"");
	    }
		
	    
    }
    
    public function clases(){
	    $clases = Clase::whereHas('horarios')->orderBy('nombre', 'asc')->get();
	    return view('site.clases', compact('clases'));
    }
    
    public function descargaPdf(){
	    $dias = Dia::with(['horarios' => function($query){
		    $query->orderBy('hora_inicial', 'asc');
	    }, 'horarios.clases'])->get();
	    
	    $sucursales = Sucursal::all();
	    
	    $view =  View::make('pdf.horarios', compact('dias', 'sucursales'))->render();
	    $pdf = App::make('dompdf.wrapper');
        $pdf->loadHTML($view);
        $pdf->setPaper('A4', 'landscape');
        $pdf->setOptions(['isHtml5ParserEnabled' => true, 'isRemoteEnabled' => true]);
	    
	    $nombre_pdf = 'HORARIO-CLASES.pdf';
	    $ruta = public_path('storage/horario/'.$nombre_pdf);
	    
		$pdf->save($ruta);
        
        return response()->download(public_path('storage/horario/'.$nombre_pdf))->deleteFileAfterSend(true);
    }
    
    
}
