<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Dia;
use App\Models\Sucursal;
use App\Models\Clase;
use App\Models\Horario;

use App\Http\Requests\Admin\HorariosRequest;

use App\Traits\DecryptValues;

use DB;

class HorariosController extends Controller
{
	use DecryptValues;
	
    public function index(){
	    $dias = Dia::with(['horarios' => function($query){
		    $query->orderBy('hora_inicial', 'asc');
	    }, 'horarios.clases'])->get();
	    $sucursales = Sucursal::all();
	    $clases = Clase::orderBy('nombre', 'asc')->get();
	    
	    return view('admin.horarios.index')->with(compact('dias', 'sucursales', 'clases'));
    }
    
    public function save(HorariosRequest $request){
	    DB::beginTransaction();
	    
	    $horario = new Horario;
	    
	    $horario->hora_inicial = $request->hora_inicial;
	    $horario->hora_final = $request->hora_final;
	    
	    $dia_id = $this->decryptValue($request->dia_id);
	    
	    if(!$dia_id){
		    return back()->withErrors('El día seleccionado no es válido');
	    }
	    
	    $sucursal_id = $this->decryptValue($request->sucursal_id);
	    
	    if(!$sucursal_id){
		    return back()->withErrors('La sucursal seleccionada no es válida');
	    }
	    
	    $horario->dia_id = $dia_id;
	    $horario->sucursal_id = $sucursal_id;
	    
	    $horario->save();
	    
	    $i = 0;
	    foreach($request->clases as $clase){
		    $clase = $this->decryptValue($clase);
		    
		    if(!$clase){
			    DB::rollBack();
			    
			    return back()->withErrors('Una clase seleccionada no es válida');
		    }
		    
		    $horario->clases()->attach($clase, ['sala' => $request->salas[$i]]);
		    $i++;
	    }
	    
	    DB::commit();
	    
	    return back()->with('mensaje', 'Se guardó el horario correctamente');
    }
    
    public function delete(Request $request){
	    $id = $request->id;
	    
	    if(empty($id)){
		    return response()->json('El registro seleccionado no es válido', 400);
	    }
	    
	    $id = $this->decryptValue($id);
	    if(!$id){
		    return response()->json('El registro seleccionado no es válido', 400);
	    }
	    
	    $horario = Horario::find($id);
	    
	    if(!$horario){
		    return response()->json('No se encontró el registro seleccionado', 400);
	    }
	    
	    $horario->delete();
	    
	    return response()->json(array('estado' => 0, 'mensaje' => 'Se eliminó exitosamente'));
    }
}
