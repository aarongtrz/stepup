<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Maestro;
use App\Models\Clase;
use App\Models\Sucursal;

use App\Http\Requests\Admin\MaestrosRequest; 

use App\Traits\DecryptValues;
use DB;
use Storage;

class MaestrosController extends Controller
{
	use DecryptValues;
	
    public function index(){
	    $maestros = Maestro::orderBy('nombre', 'asc')->paginate(15);
	    
		return view('admin.maestros.index')->with(compact('maestros'));
    }
    
    public function edit(Request $request){
	    $id = $request->id;
	    $maestro = null;
	    if(!empty($id)){
			$id = $this->decryptValue($id);
			
			if(!$id){
				return back()->withErrors('El registro seleccionado no es válido');
			} 
			
			$maestro = Maestro::where('id', $id)->first();
	    }
	    
	    $clases = Clase::all();
	    $sucursales = Sucursal::all();
	    
	    return view('admin.maestros.edit')->with(compact('maestro', 'clases', 'sucursales'));
    }
    
    public function save(MaestrosRequest $request){
	    $id = $request->id;
	    DB::beginTransaction();
	    
	    $maestro = new Maestro;
	    if(!empty($id)){
			$id = $this->decryptValue($id);
			
			if(!$id){
				DB::rollBack();
				return back()->withErrors('El registro seleccionado no es válido');
			}
			
			$maestro = Maestro::where('id', $id)->first();
			if(!$maestro){
				DB::rollBack();
				return back()->withErrors('El registro seleccionado no es válido');
			}   
	    }
	    
	    $nombre = $request->nombre;
	    $sucursal = $this->decryptValue($request->sucursal);
	    $clases = $request->clases;
	    $descripcion = $request->descripcion;
	    
	    if(!$sucursal){
		    $sucursal_model = Sucursal::where('id', $sucursal)->first();
		    if(!$sucursal_model){
			    DB::rollBack();
			    return back()->withErrors('La sucursal elegida no es válida');
		    }
	    }
	    
	    
	    $maestro->nombre = $nombre;
	    $maestro->sucursal_id = $sucursal;
	    $maestro->descripcion = $descripcion;
	    $maestro->save();
	    
	    if(!$maestro->foto && !$request->hasFile('foto')){
		    DB::rollBack();
		    return back()->withErrors('Es necesario subir una foto');
	    }
	    
	    $maestro->clases()->detach();
	    
	    foreach($clases as $clase){
		    $clase = $this->decryptValue($clase);
		    
		    if(!$clase){
			    $clase_model = Clase::where('id', $clase)->first();
			    DB::rollBack();
			    return back()->withErrors('Ocurrió un error al guardar la clase '. $clase_model->nombre);
		    }
		    
		    $maestro->clases()->attach($clase);
	    }
	    
	    if($request->hasFile('foto')){
		    if($maestro->foto){
				if(Storage::disk('maestros')->exists($maestro->foto)){
				    Storage::disk('maestros')->delete($maestro->foto);
			    }
			}
		    
		    $nombre_imagen = 'MAESTRO-'.$maestro->id.time().'.'.$request->file('foto')->extension();
	    
		    $request->foto->storeAs('', $nombre_imagen, 'maestros');
		    
		    $maestro->foto = $nombre_imagen;
		    
		    $maestro->save();
	    }
	    
	    
	    DB::commit();
	    
	    return redirect()->route('admin.maestros.edit', ['id' => encrypt($maestro->id)])->with('mensaje', 'El registró se guardó correctamente');
    }
    
    public function delete(Request $request){
	    $id = $request->id;
	    
	    if(empty($id)){
		    return response()->json('El registro seleccionado no es válido', 400);
	    }
	    
	    $id = $this->decryptValue($id);
	    if(!$id){
		    return response()->json('El registro seleccionado no es válido', 400);
	    }
	    
	    $maestro = Maestro::find($id);
	    
	    if(!$maestro){
		    return response()->json('No se encontró el registro seleccionado', 400);
	    }
	    
	    if(Storage::disk('maestros')->exists($maestro->foto)){
		    Storage::disk('maestros')->delete($maestro->foto);
	    }
	    
	    $maestro->delete();
	    
	    return response()->json(array('estado' => 0, 'mensaje' => 'Se eliminó exitosamente'));
    }
}
