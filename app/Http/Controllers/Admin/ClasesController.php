<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Clase;

use App\Http\Requests\Admin\ClasesRequest; 

use App\Traits\DecryptValues;

use DB;
use Storage;

class ClasesController extends Controller
{
	use DecryptValues;
	
    public function index(){
	    $clases = Clase::orderBy('nombre','asc')->paginate(50);
	    
	    return view('admin.clases.index')->with(compact('clases'));
    }
    
    public function edit(Request $request){
	    $id = $request->id;
	    $clase = null;
	    if(!empty($id)){
			$id = $this->decryptValue($id);
			
			if(!$id){
				return back()->withErrors('El registro seleccionado no es válido');
			} 
			
			$clase = Clase::where('id', $id)->first();  
	    }
	    
	    return view('admin.clases.edit')->with(compact('clase'));
    }
    
    public function save(ClasesRequest $request){
	    $id = $request->id;
	    DB::beginTransaction();
	    
	    $clase = new Clase;
	    if(!empty($id)){
			$id = $this->decryptValue($id);
			
			if(!$id){
				DB::rollBack();
				return back()->withErrors('El registro seleccionado no es válido');
			}
			
			$clase = Clase::where('id', $id)->first();
			if(!$clase){
				DB::rollBack();
				return back()->withErrors('El registro seleccionado no es válido');
			}   
	    }
	    
	    $nombre = $request->nombre;
	    $detalle = $request->detalle;
	    $descripcion = $request->descripcion;
	    
	    $clase->nombre = $nombre;
	    $clase->detalle = $detalle;
	    $clase->descripcion = $descripcion;
	    $clase->save();
	    
	    if($request->hasFile('foto')){
		    if($clase->foto){
				if(Storage::disk('clases')->exists($clase->foto)){
				    Storage::disk('clases')->delete($clase->foto);
			    }
			}
		    
		    $nombre_imagen = 'CLASE-'.$clase->id.time().'.'.$request->file('foto')->extension();
	    
		    $request->foto->storeAs('', $nombre_imagen, 'clases');
		    
		    $clase->foto = $nombre_imagen;
		    
		    $clase->save();
	    }
	    
	    DB::commit();
	    
	    return redirect()->route('admin.clases.edit', ['id' => encrypt($clase->id)])->with('mensaje', 'El registró se guardó correctamente');
    }
    
    public function delete(Request $request){
	    $id = $request->id;
	    
	    if(empty($id)){
		    return response()->json('El registro seleccionado no es válido', 400);
	    }
	    
	    $id = $this->decryptValue($id);
	    if(!$id){
		    return response()->json('El registro seleccionado no es válido', 400);
	    }
	    
	    $clase = Clase::find($id);
	    
	    if(!$clase){
		    return response()->json('No se encontró el registro seleccionado', 400);
	    }
	    
	    $clase->delete();
	    
	    return response()->json(array('estado' => 0, 'mensaje' => 'Se eliminó exitosamente'));
    }
    
}
