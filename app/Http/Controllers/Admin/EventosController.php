<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Evento;

use App\Http\Requests\Admin\EventosRequest; 

use App\Traits\DecryptValues;
use DB;
use Storage;

class EventosController extends Controller
{
	use DecryptValues;
	
    public function index(){
	    $eventos = Evento::orderBy('titulo', 'asc')->paginate(15);
	    
		return view('admin.eventos.index')->with(compact('eventos'));
    }
    
    public function edit(Request $request){
	    $id = $request->id;
	    $evento = null;
	    if(!empty($id)){
			$id = $this->decryptValue($id);
			
			if(!$id){
				return back()->withErrors('El registro seleccionado no es válido');
			} 
			
			$evento = Evento::where('id', $id)->first();
	    }
	    
	    return view('admin.eventos.edit')->with(compact('evento'));
    }
    
    public function save(EventosRequest $request){
	    $id = $request->id;
	    DB::beginTransaction();
	    
	    $evento = new Evento;
	    if(!empty($id)){
			$id = $this->decryptValue($id);
			
			if(!$id){
				DB::rollBack();
				return back()->withErrors('El registro seleccionado no es válido');
			}
			
			$evento = Evento::where('id', $id)->first();
			if(!$evento){
				DB::rollBack();
				return back()->withErrors('El registro seleccionado no es válido');
			}   
	    }
	    
	    $titulo = $request->titulo;
	    $descripcion = $request->descripcion;
	    
	    
	    $evento->titulo = $titulo;
	    $evento->descripcion = $descripcion;
	    $evento->save();
	    
	    if(!$evento->foto && !$request->hasFile('foto')){
		    DB::rollBack();
		    return back()->withErrors('Es necesario subir una foto');
	    }

	    
	    if($request->hasFile('foto')){
		    if($evento->foto){
				if(Storage::disk('eventos')->exists($evento->foto)){
				    Storage::disk('eventos')->delete($evento->foto);
			    }
			}
		    
		    $nombre_imagen = 'EVENTO-'.$evento->id.time().'.'.$request->file('foto')->extension();;
	    
		    $request->foto->storeAs('', $nombre_imagen, 'eventos');
		    
		    $evento->foto = $nombre_imagen;
		    
		    $evento->save();
	    }
	    
	    
	    DB::commit();
	    
	    return redirect()->route('admin.eventos.edit', ['id' => encrypt($evento->id)])->with('mensaje', 'El registró se guardó correctamente');
    }
    
    public function delete(Request $request){
	    $id = $request->id;
	    
	    if(empty($id)){
		    return response()->json('El registro seleccionado no es válido', 400);
	    }
	    
	    $id = $this->decryptValue($id);
	    if(!$id){
		    return response()->json('El registro seleccionado no es válido', 400);
	    }
	    
	    $evento = Evento::find($id);
	    
	    if(!$evento){
		    return response()->json('No se encontró el registro seleccionado', 400);
	    }
	    
	    if(Storage::disk('eventos')->exists($evento->foto)){
		    Storage::disk('eventos')->delete($evento->foto);
	    }
	    
	    $evento->delete();
	    
	    return response()->json(array('estado' => 0, 'mensaje' => 'Se eliminó exitosamente'));
    }
}
