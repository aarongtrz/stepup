<?php
namespace App\Traits;

use Illuminate\Contracts\Encryption\DecryptException; 

trait DecryptValues{
	
	
	public function decryptValue($value){
		try {
            $id = decrypt($value);
            return $id;
        } catch (DecryptException $e) {
            return false;
        }
	}
}
?>