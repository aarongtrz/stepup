<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    protected $table = 'sucursales';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
    
    public function maestros(){
	    return $this->hasMany('App\Models\Maestros', 'maestro_id');
    }
    
    public function horarios(){
	    return $this->hasMany('App\Models\Horario', 'sucursal_id');
    }
}
