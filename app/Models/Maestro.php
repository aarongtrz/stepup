<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Maestro extends Model
{
    protected $table = 'maestros';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'foto', 'sucursal_id', 'descripcion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
    
    public function clases(){
	    return $this->belongsToMany('App\Models\Clase', 'clases_maestros', 'maestro_id', 'clase_id');
    }
    
    public function sucursal(){
	    return $this->belongsTo('App\Models\Sucursal', 'sucursal_id');
    }
}
