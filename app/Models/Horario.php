<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    protected $table = 'horarios';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'hora_inicial', 'hora_final', 'sala', 'sucursal_id', 'clase_id', 'dia_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
    
    public function dia(){
	    return $this->belongsTo('App\Models\Dia', 'dia_id');
    }
    
    public function clases(){
	    return $this->belongsToMany('App\Models\Clase', 'clases_horarios', 'horario_id', 'clase_id')->withPivot('sala');
    }
    
    public function sucursal(){
	    return $this->belongsTo('App\Models\Sucursal', 'sucursal_id');
    }
}
