<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Clase extends Model
{
	protected $table = 'clases';
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
         'nombre', 'detalle', 'foto', 'descripcion'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $dates = [
        'created_at', 'updated_at',
    ];
    
    
    public function maestros(){
	    return $this->belongsToMany('App\Models\Maestro', 'clases_maestros', 'clase_id', 'maestro_id');
    }
    
    public function horarios(){
	    return $this->belongsToMany('App\Models\Horario', 'clases_horarios', 'clase_id', 'horario_id')->withPivot('sala');
    }
}
